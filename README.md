
# **Angular Image Gallery** - [Demo](http://photo-gallery-kt.herokuapp.com/)

A simple image gallery app implemented in Angular. Uses REST calls to store and retrieve images from an AWS s3 bucket. 


# Front End Requirements
- Angular 7+
- npm

Instructions to run:
- Use npm to install dependencies
- Fill in url for backend server and s3 under src/app/_services/image.service.ts
- run ng serve to start the application.


# Back End Requirement
- Java 8+
- Spring boot
- Maven
- Mongo
- AWS account
- Docker - *optional*

Instructions to run:
- Create a new bucket in AWS S3
- Add your S3 bucket and token credentials in src/main/resources/application.properties
- Add Mongo URI path in src/main/resources/application.properties
- If using Maven:
	- Install maven dependencies
	- Run as a Spring Boot application.
- If using Docker:
	- Create a Docker container using the provided Dockerfile.
	- Run the container. *(Commands to build and run the container  are inside the Dockerfile.)*
