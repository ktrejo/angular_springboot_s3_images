import { PipeTransform, Pipe } from "@angular/core";

@Pipe({ name: 'uploadCountFilter' })
export class MockUploadCountFilter implements PipeTransform {
  transform(list, expected, update) {
    return 0;
  }
}

@Pipe({ name: 'uploadStatusFilter' })
export class MockUploadStatusFilter implements PipeTransform {
  transform(parlist, expected, updateam) {
    return [];
  }
}