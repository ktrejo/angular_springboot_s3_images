import { defer } from "rxjs"

function asyncResponse(data) {
    return defer(() => Promise.resolve(data));
}

export const mockAuthenticationService = {
    logout() {
        return null;
    }

}