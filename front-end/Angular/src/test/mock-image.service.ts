import { defer } from "rxjs"

function asyncResponse(data) {
    return defer(() => Promise.resolve(data));
}

export const mockImageService = {
    getImageUrl() {
        return "mock image service test url";
    },
    getUserPhotoInfo() {
        let data = {
            "UserPhoto": {
                "id": "54c3c51c-8592-487c-a4f8-64df0aee332e",
                "username": "ktrejo",
                "folderName": "54c3c51c-8592-487c-a4f8-64df0aee332e",
                "allPhotos": {
                    "bd8d4e1c-4c92-40d5-928a-aa29894c0658": {
                        "id": "bd8d4e1c-4c92-40d5-928a-aa29894c0658",
                        "urlId": "bd8d4e1c-4c92-40d5-928a-aa29894c0658.png",
                        "name": "Vue.png",
                        "owner": "ktrejo",
                        "timestamp": "2020-02-23T19:51:58.269Z"
                    },
                    "910d2416-b0e6-4afc-bf09-719dcd6744b7": {
                        "id": "910d2416-b0e6-4afc-bf09-719dcd6744b7",
                        "urlId": "910d2416-b0e6-4afc-bf09-719dcd6744b7.png",
                        "name": "React.png",
                        "owner": "ktrejo",
                        "timestamp": "2020-02-23T19:52:09.981Z"
                    },
                    "0e4b9455-3ed0-4662-af1c-626bd6793bc4": {
                        "id": "0e4b9455-3ed0-4662-af1c-626bd6793bc4",
                        "urlId": "0e4b9455-3ed0-4662-af1c-626bd6793bc4.jpg",
                        "name": "ember-js.jpg",
                        "owner": "ktrejo",
                        "timestamp": "2020-02-23T19:52:26.971Z"
                    },
                    "c7885781-080c-4794-95d9-994a3ac884c2": {
                        "id": "c7885781-080c-4794-95d9-994a3ac884c2",
                        "urlId": "c7885781-080c-4794-95d9-994a3ac884c2.png",
                        "name": "Angular.png",
                        "owner": "ktrejo",
                        "timestamp": "2020-02-23T19:52:44.790Z"
                    },
                    "6f8475b6-c258-4c03-8c0f-ac195ddbddd8": {
                        "id": "6f8475b6-c258-4c03-8c0f-ac195ddbddd8",
                        "urlId": "6f8475b6-c258-4c03-8c0f-ac195ddbddd8.jpg",
                        "name": "biddulph-grange-4.jpg",
                        "owner": "ktrejo",
                        "timestamp": "2020-04-10T03:54:26.825Z"
                    },
                    "fe911fa2-301b-4084-ad61-46e385382ee9": {
                        "id": "fe911fa2-301b-4084-ad61-46e385382ee9",
                        "urlId": "fe911fa2-301b-4084-ad61-46e385382ee9.jpg",
                        "name": "redkite50498.jpg",
                        "owner": "ktrejo",
                        "timestamp": "2020-04-13T04:45:34.685Z"
                    },
                    "1381d8b4-d9b8-473d-8755-a48214e681d0": {
                        "id": "1381d8b4-d9b8-473d-8755-a48214e681d0",
                        "urlId": "1381d8b4-d9b8-473d-8755-a48214e681d0.jpg",
                        "name": "beautiful-images-wallpapers-(1).jpg",
                        "owner": "ktrejo",
                        "timestamp": "2020-04-15T02:52:21.242Z"
                    }
                },
                "albums": [
                    {
                        "id": "201a3efc-3ed7-4cb6-9757-3825f5c256ea",
                        "albumName": "test",
                        "timestamp": "2020-02-23T19:59:18.770Z"
                    }
                ]
            }
        }

        return asyncResponse(data);
    }
}