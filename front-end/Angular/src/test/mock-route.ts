import { Component } from "@angular/core";

@Component({
    template:''
})
export class BlankComponent {

}

export const routes = [
    {
      path: '', component: BlankComponent
    },
    {
      path: 'login', component: BlankComponent
    },
    {
      path: 'upload', component: BlankComponent
    },
    {
      path: 'view', component: BlankComponent
    },
    {
      path: 'logout', component: BlankComponent
    }
  ]