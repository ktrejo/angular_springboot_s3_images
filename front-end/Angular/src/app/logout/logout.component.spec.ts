import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BlankComponent, routes } from 'src/test/mock-route';
import { mockAuthenticationService } from 'src/test/mock-authentication.service';
import { AuthenticationService } from '../_services/authentication.service';

import { LogoutComponent } from './logout.component';


describe('LogoutComponent', () => {
  let component: LogoutComponent;
  let fixture: ComponentFixture<LogoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LogoutComponent,
        BlankComponent
      ],
      imports: [
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        {
          provide: AuthenticationService,
          useValue: mockAuthenticationService
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
