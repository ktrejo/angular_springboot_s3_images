import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { filter, map } from 'rxjs/operators';


@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Token Expired!</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p style="letter-spacing: 1px;">Your current token expired. Please login again to access your account.</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
    </div>
  `
})
export class TokenExpireModalContent {

  constructor(public activeModal: NgbActiveModal) {}
}




@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    submitted: boolean = false;
    loadingIcon: boolean = false;
    navigateUrl: string = '';
    error: string = '';
    tokenExpired: boolean = false;

    loginInfo = {
        title: "Login",
        button: "Login",
        registerOption: true
    }
    registerInfo = {
        title: "Register",
        button: "Submit",
        registerOption: false
    }
    formInfo = this.loginInfo;

    constructor(private formBuilder: FormBuilder, 
                private router: Router, 
                private authService: AuthenticationService,
                private modalService: NgbModal) {
    }


    ngOnInit() {
        let state = this.router.events.pipe(
            filter(e => e instanceof NavigationStart),
            map(() => this.router.getCurrentNavigation().extras.state)
        )

        if (state && state['token']) {
            this.tokenExpired = true
        }


        if (this.router.url.replace("/", "") !== "login") {
            this.formInfo = this.registerInfo;
        }

        this.loginForm = this.formBuilder.group({
            username: ['', [Validators.required]],
            password: ['', Validators.required]
        });

        // logout user to prevent double login
        this.authService.logout();

        if (this.tokenExpired === true) {
            this.openTokenExpireMessage();
        }

    }

    openTokenExpireMessage() {
        setTimeout(() => {
            let modalRef = this.modalService.open(TokenExpireModalContent);
        }, 0);
    }

    onSubmit() {
        
        this.submitted = true;

        // return if form is invalid
        if (this.loginForm.invalid) {
            this.error = '';
            return;
        }

        this.loadingIcon = true;

        (this.formInfo.registerOption === true)? this.login() : this.register();

    }

    login() {
         // Authenticate user
        this.authService.login(this.loginForm.controls.username.value, 
                                this.loginForm.controls.password.value).subscribe(

            data => {
                this.router.navigate([this.navigateUrl]);
            },
            error => {
                this.loadingIcon = false;
                this.error = "Invalid Credentials";
            });

    }

    register() {
        // Register user
        this.authService.register(this.loginForm.controls.username.value, 
                                this.loginForm.controls.password.value).subscribe(

            data => {
                this.router.navigate([this.navigateUrl]);
            },
            error => {
                this.loadingIcon = false;
                this.error = "User already exist";
            });


    }

}
