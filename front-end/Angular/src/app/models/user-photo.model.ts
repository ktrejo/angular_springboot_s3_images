import { UserAlbumBasic } from '../models/user-album.model';
import {Photo, extractPhoto} from '../models/photo.model';

export class UserPhoto {
    id: number;
    username: string;
    folderName: string;
    allPhotos: Photo[];
    albums: UserAlbumBasic[];

    constructor(jsonUserPhoto) {
        var jsonData = jsonUserPhoto['UserPhoto'];
        
        this.id = jsonData['id'];
        this.username = jsonData['username'];
        this.folderName = jsonData['folderName'];
        this.allPhotos = extractPhoto(jsonData['allPhotos']);
        this.albums = [];

        for(let value of Object.values(jsonData['albums'])){
            this.albums.push(new UserAlbumBasic(value));
        }

    }
}