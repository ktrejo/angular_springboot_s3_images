export class Photo {
    id: string;
    urlId: string;
    name: string;
    owner: string;
    timestamp: string;

    constructor(data) {
        this.id = data['id'];
        this.urlId = data['urlId']
        this.name = data['name'];
        this.owner = data['owner']
        this.timestamp = data['timestamp']
    }
}

export function extractPhoto(data): Photo[] {
    var values: Object[] = Object.values(data);
    var result: Photo[] = new Array(values.length);
    values.forEach((photoObject, idx) => {
        result[idx] = new Photo(photoObject);
    });

    return result;
}