import {Photo, extractPhoto} from '../models/photo.model';

export class UserAlbumBasic {
    id: number;
    albumName: string;
    timestamp: string;

    constructor(data) {
        this.id = data['id'];
        this.albumName = data['albumName'];
        this.timestamp = data['timestamp'];
    }
}

export class UserAlbum extends UserAlbumBasic {
    owner: string;
    folderName: string;
    sharedTurnOn: boolean;
    allPhotos: Photo[];
    sharedUsers: string[];

    constructor(data) {
        super(data);
        this.owner = data['owner'];
        this.folderName = data['folderName'];
        this.sharedTurnOn = data['sharedTurnOn'];
        this.allPhotos = extractPhoto(data['allPhotos']);
        this.sharedUsers = data['sharedUsers'];
    }
}