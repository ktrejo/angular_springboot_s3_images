import { Component } from '@angular/core';
import { AuthGuardService } from './_services/auth-guard.service';
import { LoginComponent, TokenExpireModalContent } from './login/login.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'Image Bucket';
    userLogin = true;
    isNavbarCollapsed=true;

    constructor(public authGuard : AuthGuardService) {}

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }


    onActivate(event: object) {
        this.isNavbarCollapsed = true;

        if (event instanceof LoginComponent || 
            event instanceof TokenExpireModalContent) {

            this.userLogin = false;

        } else {
            this.userLogin = true;
        } 

    }
}
