import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { ImageService } from '../_services/image.service';
import { IAlbum } from 'ngx-lightbox';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

import { UserPhoto } from '../models/user-photo.model';
import { Photo } from '../models/photo.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    // Message to display at the top.
    headerMessage: string = "Recent Images";

    // Object containing image info
    recentImages: IAlbum[] = [];


    constructor(private imageService: ImageService,
                private router: Router, 
                private carouselConfig: NgbCarouselConfig) {

        this.carouselConfig.interval = 3000;
        this.carouselConfig.wrap = true;
        this.carouselConfig.showNavigationArrows = false;
    }

    ngOnInit() {
        this.loadRecentImages();
    }


    /*
    * Acquire the past 3 recent images uploaded to the gallery 
    * and update headerMessage.
    */
    loadRecentImages() {
        this.imageService.getUserPhotoInfo().subscribe((data) => {

            let userPhoto = new UserPhoto(data);

            let imageBaseUrl = this.imageService.getImageUrl() + userPhoto.folderName + '/';
            let imageNames: Photo[] = userPhoto.allPhotos;
            imageNames = imageNames.slice(-3).reverse();

            if (imageNames.length == 0) {
                this.headerMessage = "Gallery is Currently Empty";
            }

            this.recentImages = imageNames.map((photo) => {
                return {
                    src: imageBaseUrl + photo.urlId,
                    caption: photo.id,
                    thumb: null
                }
            });

        }, (error) => {
            if (error && error.status === 403) {
                this.router.navigate(['/login'], { state: {token: 'expired'}});
            } else {
                this.headerMessage = "Unable to Retrieve Images";
                console.log("App could not reach server");
            }
        });
    }

}
