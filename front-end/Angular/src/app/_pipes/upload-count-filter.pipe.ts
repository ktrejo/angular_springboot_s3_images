import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'uploadCountFilter',
  pure: false
})
export class UploadCountFilterPipe implements PipeTransform {

  transform(items: any[], expectedStatus?: string, update?: any[]): Number {
    if (!items) {
      return 0;
    }

    if (!expectedStatus || !update) {
      return items.length;
    }

    return items.reduce((sum, item) => {
      if (item.status === expectedStatus) {
        sum++;
      }

      return sum;
    }, 0);

  }

}
