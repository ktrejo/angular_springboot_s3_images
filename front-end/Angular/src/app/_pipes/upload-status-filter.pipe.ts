import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'uploadStatusFilter',
  pure: false
})
export class UploadStatusFilterPipe implements PipeTransform {

  transform(items: any[], expectedStatus?: string, update?: any[]): any[] {
    if (!items) {
      return [];
    }

    if (!expectedStatus || !update) {
      return items;
    }

    return items.filter(item => {
      return (item.status === expectedStatus);
    }); 
  }

}
