import { Component, OnInit, Renderer2, ElementRef} from '@angular/core';
import { Router } from '@angular/router';
import { ImageService } from '../_services/image.service';
import { Lightbox, LightboxConfig, IAlbum } from 'ngx-lightbox';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { UserPhoto } from '../models/user-photo.model';
import { UserAlbum, UserAlbumBasic } from '../models/user-album.model'
import { CreateAlbumTemplate, AddPhotoToAlbumTemplate, RemovePhotoAlbumTemplate, 
         DeleteAlbumTemplate, ShareAlbumTemplate } from './templates/templates';
import { Photo } from '../models/photo.model';


@Component({
    selector: 'app-image-viewer',
    templateUrl: './image-viewer.component.html',
    styleUrls: ['./image-viewer.component.css']
})
export class ImageViewerComponent implements OnInit {

    // contains user's photo gallery info
    private userPhoto: UserPhoto;

    // Contains all image information   
    private images: IAlbum[] = [];

    // contains current album info
    currAlbum: UserAlbum = null;

    // images to modify
    imagesToModify: Map<string, ElementRef> = new Map();

    // length of images array
    imageLength: number = 0;

    // Selected range of images
    imageSubset: IAlbum[] = [];

    // contains user's album references, both private and shared
    albums:any[] = [];

    // Current page number when component is first display
    page: number = 1;

    // Number of images per page
    pageSize: number = 15;

    // Message to display at the top.
    headerMessage: string = '';

    // Turn on edit option
    editEnable: boolean = false;


    constructor(private imageService: ImageService, 
        private router: Router,
        private renderer: Renderer2,
        private lightbox: Lightbox,
        private lightboxConfig: LightboxConfig,
        private modalService: NgbModal) {}


    /**
    * Set lightbox configuration and retrieve location for images from server
    */
    ngOnInit() {
        this.lightboxConfig.centerVertically = true;
        this.lightboxConfig.wrapAround = true;
        this.lightboxConfig.showImageNumberLabel = true;

        this.loadImages();
    }


    /**
    * Obtain the image urls from image service
    */
    loadImages() {
        this.imageService.getUserPhotoInfo().subscribe((data) => {
            this.setUserPhotoAndAlbums(data);
            let imageBaseUrl = this.imageService.getImageUrl() + this.userPhoto.folderName + '/';
            this.updateLightbox(this.userPhoto.allPhotos, imageBaseUrl, this.albums[0].albumName);

        }, (error) => {
            this.errorHandler(error);
        });
    }

    /**
    * update the user photo and album information
    */
    setUserPhotoAndAlbums(data) {
        this.userPhoto = new UserPhoto(data);
        this.albums = [];
        this.albums.push({ albumName: 'Your Gallery', albumId: -1 });
        this.userPhoto.albums.forEach((value) => {
            let temp = { albumName: value.albumName, albumId: value.id };
            this.albums.push(temp);
        });
    }


    /**
    * Navigate photo gallery view to different albums or
    * all photos of the user
    */
    changeAlbum(album) {
        
        // All your photo. Not an album
        if (album.albumId === -1) {
            this.headerMessage = album.albumName;
            this.currAlbum = null;
            let imageBaseUrl = this.imageService.getImageUrl() + this.userPhoto.folderName + '/';
            this.updateLightbox(this.userPhoto.allPhotos, imageBaseUrl, this.albums[0].albumName);
            
        } else {
           this.imageService.getAlbum(album.albumId).subscribe((data) => {
               this.loadAlbum(data);
           }, (error) => {
               this.errorHandler(error);
           }); 
        }

    }


    /**
    * Load a private or shared album
    */
    loadAlbum(data) {
        this.currAlbum = new UserAlbum(data['Album']);
        this.headerMessage = this.currAlbum.albumName;

        let imageBaseUrl = this.currAlbum.folderName + '/';
        if (this.currAlbum.sharedTurnOn) {
            imageBaseUrl = 'album/' + imageBaseUrl;
        }
        imageBaseUrl = this.imageService.getImageUrl() + imageBaseUrl

        this.updateLightbox(this.currAlbum.allPhotos, imageBaseUrl, this.currAlbum.albumName);
    }


    /*
    * Generate the Album objects for lightbox display.
    */
    updateLightbox(images: Photo[], baseUrl, headerMessage) {

        this.images = images.map((photo, index) => {
            let image = {
                src: baseUrl + photo.urlId,
                src_id: photo.id,
                caption: photo.name,
                thumb: null
            };

            return image
        }).reverse();

        this.imageLength = this.images.length;
        this.headerMessage = headerMessage;

        if (this.imageLength == 0) {
            this.headerMessage += ' is Empty';
        }

        this.updateLightboxSubset(1, this.pageSize);
    }


    /**
    * Updates page number, pageSize and image range for bootstrap pagination
    * and lightbox.
    */
    updateLightboxSubset(page, pageSize) {
        this.page = page;
        this.pageSize = pageSize;
        let range = (this.page - 1) * this.pageSize;
        this.imageSubset = this.images.slice(range, range + this.pageSize);

        this.imageSubset = this.images.slice((this.page - 1) * this.pageSize, 
            (this.page - 1) * this.pageSize + this.pageSize);

    }


    /**
    * Change the range of images depending on the page number. Also move the
    * scroll bar location back to the top.
    */
    loadPage($event) {
        this.updateLightboxSubset($event, this.pageSize); 

        window.scroll(0,0);
    }


    /**
    * Open a lightbox with an index representing the selected image.
    */
    openLightbox(index: number) {
        this.lightbox.open(this.imageSubset, index);
    }


    /**
    * Close current lightbox
    */
    closeLightbox() {
        this.lightbox.close();
    }

    
    /**
    * Opens ngbModal. Passes the desire template, modalContent, to modalService.
    */
    openModal(modalName) {
        let modalMap = {
            'createAlbumTemplate': CreateAlbumTemplate,
            'addPhotosToAlbumTemplate': AddPhotoToAlbumTemplate,
            'deletePhotoTemplate': RemovePhotoAlbumTemplate,
            'deleteAlbumTemplate': DeleteAlbumTemplate,
            'shareAlbumTemplate': ShareAlbumTemplate
        }

        let modalRef = this.modalService.open(modalMap[modalName], {backdrop: 'static'});
        modalRef.componentInstance.imageViewer = {
            albums: this.albums,
            currAlbum: this.currAlbum,
            currUser: this.userPhoto.username
        };

        modalRef.result.then(
            (result) => {

                switch(modalName) {

                    case 'createAlbumTemplate':
                        this.createNewAlbum(result);
                        break;

                    case 'addPhotosToAlbumTemplate':
                        this.addPhotosToAlbum(result);
                        break;

                    case 'shareAlbumTemplate':
                        let obj = {
                            albumName: this.currAlbum.albumName,
                            albumId: this.currAlbum.id
                        }
                        this.changeAlbum(obj);
                        break;

                    case 'deletePhotoTemplate':
                        this.deleteImages();
                        break;

                    case 'deleteAlbumTemplate':
                        this.toggleEdit();
                        this.currAlbum = null;
                        this.loadImages();
                        break;

                    default:
                        let msg = result.templateName + ' action not defined'; 
                        console.log(msg);

                }


            }, (dismiss) => {
                // action for dismiss

                // Handle the http service error that occur inside the modal.
                // Most errors would be the result of expired token.
                if (dismiss && dismiss.error) {
                    this.errorHandler(dismiss.error);
                }
            });
    }


    /**
    * Create a new album for the user. By default sharing is turned off
    */
    createNewAlbum(result) {
        let albumName = result.name;
        if (albumName === undefined || albumName.length === 0){
            return;
        }

        this.imageService.createAlbum(albumName).subscribe((data) => {
            this.setUserPhotoAndAlbums(data);
            
        }, (error) => {
            console.log("Could not create new album");
            this.errorHandler(error);
        })
    }


    /**
    * Add photos to album. NOTE: photos can successfully be added
    * only if the current user is the owner of the album. Other
    * user who have access through sharing cannot modify the album.
    */
    addPhotosToAlbum(result) {
        let albumId = result.id;
        let listOfPhotos = Array.from(this.imagesToModify.keys());
        
        if (this.editEnable) {
            this.toggleEdit();
        }
        
        this.imageService.addPhotoToAlbum(albumId, listOfPhotos).subscribe((data) => {
            this.loadAlbum(data);
        }, (error) => {
            this.errorHandler(error);
        });
        

    }


    /**
    * Deselect any selected photos
    */
    toggleEdit() {
        if (this.imagesToModify.size > 0) {
            for(let [imgName, img] of this.imagesToModify) {
                this.renderer.removeClass(img, 'img-selected');
            }
        }

        this.imagesToModify.clear();
        this.editEnable = !this.editEnable;
    }


    /**
    * Add css to images selected by user.
    */
    selectImage(targetDiv, image) {
        let spanChild = targetDiv.firstChild;
        let imgSibling = targetDiv.previousElementSibling;

        if (!this.imagesToModify.has(image.src_id)) {
            this.imagesToModify.set(image.src_id, imgSibling);

            this.renderer.setStyle(spanChild, 'display', 'initial');
            this.renderer.addClass(imgSibling, 'img-selected'); 

        } else {
            this.imagesToModify.delete(image.src_id);

            this.renderer.setStyle(spanChild, 'display', 'none');
            this.renderer.removeClass(imgSibling, 'img-selected');

        }
    }

    
    /*
    * Delete images from the photo gallery
    */
    deleteImages() {

        if (this.imagesToModify.size == 0) {
            return;
        }

        let imagesToDelete = Array.from(this.imagesToModify.keys());
        this.toggleEdit();

        // Delete call is not from an album
        if (this.currAlbum == null) {
            this.imageService.deleteImages(imagesToDelete).subscribe((data) => {
                this.setUserPhotoAndAlbums(data);
                let imageBaseUrl = this.imageService.getImageUrl() + this.userPhoto.folderName + '/';
                this.updateLightbox(this.userPhoto.allPhotos, imageBaseUrl, this.albums[0].albumName);
            }, (error) => {
                this.errorHandler(error);
            });
        
        } else {
            this.imageService.removePhotoFromAlbum(this.currAlbum.id, imagesToDelete).subscribe((data) => {
                this.loadAlbum(data);
            }, (error) => {
                this.errorHandler(error);
            });

        }
    }


    /**
    * Handle http errors that occur from using image service
    */
    errorHandler(error) {
        if (error) {
            switch(error.status) {
                case 401:
                    this.router.navigate(['/login'], { state: {token: 'expired'}});
                    break;
                case 403:
                    console.log(error);
                    break;
                default:
                    this.headerMessage = 'Unable to Retrieve Images';
                    console.log('App could not reach server');
            }
        }
    }

}



