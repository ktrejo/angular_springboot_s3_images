import { Component, OnInit, Input} from '@angular/core';
import {NgbModal, NgbActiveModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { ImageService } from '../../_services/image.service';
import { UserAlbum } from '../../models/user-album.model'


@Component({
  selector: 'create-album-modal',
  template: `
    <div class="modal-header">
      <h4 class="modal-title" id="modal-basic-title">Create Album</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form>
        <div class="form-group">
          <label for="albumName" class="roboto-text">Enter Album Name</label>
          <input id="albumName" type="text" class="form-control" placeholder="New Album" [(ngModel)]="newAlbumName" [ngModelOptions]="{standalone: true}" >
        </div>
      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-success" [disabled]="disableButton(newAlbumName)" (click)="createNewAlbum()">Create</button>
    </div>
  `,
  styleUrls: ['../image-viewer.component.css']
})
export class CreateAlbumTemplate {
    @Input() imageViewer;
    newAlbumName: string = "";

    constructor(public activeModal: NgbActiveModal) {}

    /**
    * Close the modal and return the new album name string
    */
    createNewAlbum() {
        this.newAlbumName = this.newAlbumName.trim();
        this.activeModal.close({name: this.newAlbumName});
    }

    /**
    * function to disable the create album button if the input string is empty or only has whitespace
    */
    disableButton(value: string) {
        return value.trim() === "";
    }
}

@Component({
  selector: 'add-photo-album-modal',
  template: `
    <div class="modal-header">
      <h4 class="modal-title" id="modal-basic-title">Add Photo to Album</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form>
        <div class="form-group">
          <label for="selectAlbum">Select Album</label>
          <select class="custom-select mr-sm-2" id="selectAlbum" [(ngModel)]="selAlbum" [ngModelOptions]="{standalone: true}">
            <option *ngFor="let album of imageViewer.albums | slice:1" [value]=album.albumId>{{album.albumName}}</option>
        </select>
        </div>
      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-success" [disabled]="selAlbum === undefined" (click)="addToAlbum()">Add to Album</button>
    </div>
  `,
  styleUrls: ['../image-viewer.component.css']
})
export class AddPhotoToAlbumTemplate {
    @Input() imageViewer;
    selAlbum;

    constructor(public activeModal: NgbActiveModal) {}

    /**
    * close the modal and return the album id which correspond the the desired album
    */
    addToAlbum() {
        this.activeModal.close({id: this.selAlbum});
    }

}

@Component({
  selector: 'remove-photo-album-modal',
  template: `
    <div class="modal-header">
      <h4 class="modal-title" id="modal-basic-title">Delete Photo</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cancel Delete')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>Are you sure you want to delete your photo(s)?</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-lg btn-outline-primary" (click)="activeModal.close()">Yes</button>
      <button type="button" class="btn btn-lg btn-danger" (click)="activeModal.dismiss('Cancel Delete')">No</button>
    </div>
  `,
  styleUrls: ['../image-viewer.component.css']
})
export class RemovePhotoAlbumTemplate {
    @Input() imageViewer;

    constructor(public activeModal: NgbActiveModal) {}
}


@Component({
  selector: 'delete-album-modal',
  template: `
    <div class="modal-header">
      <h4 class="modal-title" id="modal-basic-title">Delete Photo</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cancel Delete')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>Are you sure you want to delete this Album "{{imageViewer.currAlbum.albumName}}"?</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-lg btn-outline-primary" (click)="deleteAlbum()">Yes</button>
      <button type="button" class="btn btn-lg btn-danger" (click)="activeModal.dismiss('Cancel Delete')">No</button>
    </div>
  `,
  styleUrls: ['../image-viewer.component.css']
})
export class DeleteAlbumTemplate {
    @Input() imageViewer;

    constructor(public activeModal: NgbActiveModal, private imageService: ImageService) {}

    /**
    * Delete selected album
    */
    deleteAlbum() {
        let currAlbum: UserAlbum = this.imageViewer.currAlbum;
        this.imageService.deleteAlbum(currAlbum.id).subscribe((data) => {
            this.activeModal.close();
        }, (error) => {
            this.activeModal.dismiss({error: error});
        });
    }
}


@Component({
  selector: 'share-album-modal',
  template: `
    <div class="modal-header">
      <h4 class="modal-title" id="modal-basic-title">Share Album</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.close()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div *ngIf="!imageViewer.currAlbum.sharedTurnOn">
      <div class="modal-body">
        <p>Do you want to share your album? Warning: This action cannot be unchanged.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-lg btn-outline-primary" (click)="turnShareOn()">Yes</button>
        <button type="button" class="btn btn-lg btn-danger" (click)="activeModal.close()">No</button>
      </div>
    </div>

    <div *ngIf="imageViewer.currAlbum.sharedTurnOn">
      <div class="modal-body">
        <form *ngIf="!this.viewUser">
          <div class="form-group">
            <label for="newUser" class="roboto-text">Enter Username {{headerMessage === "" ? "" : "- " + headerMessage}}</label>
            <input id="newUser" type="text" class="form-control" placeholder="New User" [(ngModel)]="newUsername" [ngModelOptions]="{standalone: true}">
          </div>
          <button type="button" class="btn btn-lg" [disabled]="newUsername === ''" [ngClass]="newUsername === '' ? 'btn-outline-success' : 'btn-success'" (click)="addUser()">Add</button>
          <button type="button" class="btn btn-lg btn-info" style="margin:0 1.5em" (click)="toggleViewUser()">Show Users</button>
        </form>

        <form *ngIf="this.viewUser">
          <div class="form-group">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>Role</th>
                  <th>Username</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Owner</td>
                  <td>{{imageViewer.currAlbum.owner}}</td>
                  <td>-</td>
                </tr>
              </tbody>
              <tbody>
                <tr *ngFor="let user of imageViewer.currAlbum.sharedUsers">
                  <td>User</td>
                  <td>{{user}}</td>
                  <td>
                    <ng-container 
                        [ngTemplateOutlet]="imageViewer.currUser == imageViewer.currAlbum.owner ? userIsOwner: userIsShared"
                        [ngTemplateOutletContext]="{username: user}">
                    </ng-container>
                  </td>
                </tr>
              </tbody>
            </table>
            <button  type="button" class="btn btn-lg btn-info" *ngIf="imageViewer.currUser == imageViewer.currAlbum.owner" (click)="toggleViewUser()">Add Users</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-lg btn-primary" (click)="activeModal.close()">Finish</button>
      </div>
    </div>

    <ng-template #userIsOwner let-username="username">
      <span class="fas fa-user-minus" (click)="deleteUser(username)"></span>
    </ng-template>
    <ng-template #userIsShared>-</ng-template>
  `,
  styleUrls: ['../image-viewer.component.css']
})
export class ShareAlbumTemplate {
    @Input() imageViewer;
    headerMessage: string = "";
    newUsername: string = "";
    viewUser: boolean = false;

    constructor(public activeModal: NgbActiveModal, private imageService: ImageService) {}

    ngOnInit() {
        if (this.imageViewer.currUser != this.imageViewer.currAlbum.owner) {
            this.viewUser = true;
        }
    }

    /**
    * Enable sharing on current album
    */
    turnShareOn() {
        let currAlbum: UserAlbum = this.imageViewer.currAlbum;
        this.imageService.shareAlbumEnable(currAlbum.id).subscribe((data) => {
            this.imageViewer.currAlbum = new UserAlbum(data['Album']);
        }, (error) => {
            this.activeModal.dismiss({error: error});
        });    
    }

    
    /**
    * Add user to current album
    */
    addUser() {
        let userAlbum: UserAlbum = this.imageViewer.currAlbum;
        this.newUsername = this.newUsername.trim();
        this.imageService.shareAlbumAddUser(userAlbum.id, this.newUsername).subscribe((data)=> {
            this.headerMessage = "Done";
            this.newUsername = "";
            this.imageViewer.currAlbum = new UserAlbum(data['Album']);
        }, (error) => {
            this.activeModal.dismiss({error: error});
        });

    }

    
    /**
    * Remove user from current album
    */
    deleteUser(username) {
        let userAlbum: UserAlbum = this.imageViewer.currAlbum;
        this.imageService.shareAlbumRemoveUser(userAlbum.id, username).subscribe((data) => {
            this.imageViewer.currAlbum = new UserAlbum(data['Album']);
        }, (error) => {
            this.activeModal.dismiss({error: error});
        });
    }

    
    /**
    * toggle between entering a new user and viewer users who has access to the current album.
    */
    toggleViewUser() {
        this.newUsername = "";
        this.viewUser = !this.viewUser;
    }
}