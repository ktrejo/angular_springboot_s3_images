import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ImageService } from '../_services/image.service';
import { mockImageService } from 'src/test/mock-image.service';
import { RouterTestingModule } from '@angular/router/testing';
import { BlankComponent, routes } from 'src/test/mock-route';
import { Renderer2 } from '@angular/core';
import { Lightbox, LightboxConfig, LightboxEvent } from 'ngx-lightbox';
import { NgbModal, NgbPagination } from '@ng-bootstrap/ng-bootstrap';

import { ImageViewerComponent } from './image-viewer.component';


describe('ImageViewerComponent', () => {
  let component: ImageViewerComponent;
  let fixture: ComponentFixture<ImageViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ImageViewerComponent,
        BlankComponent,
        NgbPagination
      ],
      imports: [
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        {
          provide: ImageService,
          useValue: mockImageService
        },
        Renderer2,
        Lightbox,
        LightboxConfig,
        LightboxEvent,
        NgbModal
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
