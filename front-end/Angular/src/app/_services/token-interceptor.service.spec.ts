import { TestBed } from '@angular/core/testing';
import { AuthenticationService } from './authentication.service';
import { mockAuthenticationService } from 'src/test/mock-authentication.service';

import { TokenInterceptorService } from './token-interceptor.service';

describe('TokenInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{provide: AuthenticationService, useValue: mockAuthenticationService}]
    });
  });

  it('should be created', () => {
    const service: TokenInterceptorService = TestBed.get(TokenInterceptorService);
    expect(service).toBeTruthy();
  });
});
