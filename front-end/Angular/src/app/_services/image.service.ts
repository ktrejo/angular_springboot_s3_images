import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

    apiBaseUrl : string;
    imageBaseUrl: string;
    sharedAlbumUrl: string;

    constructor(private http: HttpClient) {
        this.apiBaseUrl = "http://localhost:5000/api";
        this.imageBaseUrl = "https://s3.amazonaws.com/**Bucket Name**/";
        this.sharedAlbumUrl = "https://s3.amazonaws.com/**Bucket Name**/album/";
    }

    
    /**
    * Return the base url where the images can be retrieve.
    */
    getImageUrl() {
        return this.imageBaseUrl;
    }


    /**
    * Returns the url for shared albums
    */
    getSharedAlbumUrl() {
        return this.sharedAlbumUrl;
    }


    /**
    * Obtains user photo gallery info. Returns an observable object.
    */
    getUserPhotoInfo() {
        return this.photoRequest("get");
    }


    /**
    * Upload an image with the file set as keyName to user photo gallery.
    * Returns an observable object. Result will be in json format. 
    */
    uploadImage(keyName: string, imageFile) {
        let data = new FormData();
        data.append("keyName", keyName);
        data.append("uploadFile", imageFile);

        let httpOptions = {
            body: data,
            observe: "events",
            reportProgress: true
        }

        return this.photoRequest("post", httpOptions);
    }


    /**
    * Delete multiple images whose names are given in listOfIds
    */
    deleteImages(listOfIds: string[]) {
        let httpOptions = {
            body: {
                listOfIds: listOfIds 
            }
        }

        return this.photoRequest("delete", httpOptions);
    }


    /**
     * Helper method for accessing/creating/deleting a photo
     */
    photoRequest(requestType, httpOptions = {}) {
        let endpoint = this.apiBaseUrl + "user/photo";

        return this.http.request(requestType, endpoint, httpOptions);
    }


    /**
    * Get information about the album given its id
    */
    getAlbum(albumId) {
        let httpOptions = {
            params: {
                "albumId": albumId
            }
        };

        return this.albumRequest("get", httpOptions);
    }
    

    /**
    * Create a new album using albumName as its name.
    */
    createAlbum(albumName) {
        let httpOptions = {
            params: {
                "albumName": albumName
            }
        };

        return this.albumRequest("post", httpOptions);
    }


    /**
    * Delete an album given the album id
    */
    deleteAlbum(albumId) {
        let httpOptions = {
            params: {
                "albumId": albumId
            }
        };

        return this.albumRequest("delete", httpOptions);
    }


    /**
     * Helper method for accessing/creating/removing an album
     */
    albumRequest(requestType, httpOptions) {
        let endpoint = this.apiBaseUrl + "user/album";

        return this.http.request(requestType, endpoint, httpOptions);
    }


    /**
    * Add photos (listOfPhotos) to the desire album (albumId)
    */
    addPhotoToAlbum(albumId, listOfPhotos) {
        return this.photoToAlbum(albumId, listOfPhotos, "post");
    }

    
    /**
    * Remove photos (listOfPhotos) from the desire album (albumId)
    */
    removePhotoFromAlbum(albumId, listOfPhotos) {
        return this.photoToAlbum(albumId, listOfPhotos, "delete");
    }


    /**
    * Helper method for adding/removing photos from an album
    */
    private photoToAlbum(albumId, listOfPhotos, requestType) {
        let endpoint = this.apiBaseUrl + "user/album/update";
        let httpOptions = {
            params: {
                "albumId": albumId
            },
            body: {
                listOfIds: listOfPhotos
            }
        };

        return this.http.request(requestType, endpoint, httpOptions);
    }


    /**
    * Enable an album to be shared. NOTE this action can only be done if the current
    * user is also the owner of the album.
    */
    shareAlbumEnable(albumId) {
        let endpoint = this.apiBaseUrl + "user/album/share";
        let httpOptions = {
            params: {
                "albumId": albumId
            }
        };

        return this.http.post(endpoint, null, httpOptions);
    }


    /**
    * Add a user to a shared album. NOTE this action can only be done if the current
    * user is also the owner of the album
    */
    shareAlbumAddUser(albumId, username) {
        return this.shareAlbumUserHelper(albumId, username, "post");
    }

    
    /**
    * Remove a user from a shared album. NOTE this action can only be done if the
    * current user is also the owner of the album.
    */
    shareAlbumRemoveUser(albumId, username) {
        return this.shareAlbumUserHelper(albumId, username, "delete");
    }

    
    /**
    * Helper method for adding/removing users from a shared album.
    */
    private shareAlbumUserHelper(albumId, username, requestType) {
        let endpoint = this.apiBaseUrl + "user/album/share/update";
        let httpOptions = {
            params: {
                "albumId": albumId,
                "username": username
            }
        };

        return this.http.request(requestType, endpoint, httpOptions);
    }
}
