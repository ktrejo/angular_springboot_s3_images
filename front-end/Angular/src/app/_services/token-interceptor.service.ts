import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpSentEvent, HttpHeaderResponse, HttpProgressEvent, HttpResponse, HttpUserEvent, HttpErrorResponse } from "@angular/common/http";
import { throwError as observableThrowError,  Observable ,  BehaviorSubject } from 'rxjs';
import { take, filter, catchError, switchMap, finalize } from 'rxjs/operators';

import { AuthenticationService } from './authentication.service';


@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

    isRefreshingToken: boolean = false;
    tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    constructor(private authService: AuthenticationService) {}

    /**
    * intercepts any http request and checks if authorization token is needed
    */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {

        if (req.url.indexOf('user/') !== -1) {
            let new_req = this.addToken(req, this.authService.getToken());
            //console.log(new_req);
            return next.handle(new_req).pipe(
                catchError(error => {

                    if (error instanceof HttpErrorResponse &&
                        (<HttpErrorResponse>error).status == 401) {

                        return this.handle401Error(error);

                    } else {
                        return observableThrowError(error);
                    }
                }));

        } else {
            return next.handle(req);
        }
        
    }

    /**
    * Add Authorization token header
    */
    addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
        return req.clone({ setHeaders: { Authorization: 'Bearer ' + token }})
    }


    /**
    * Remove stored credentials if credentials are expired
    */
    handle401Error(error) {
        if (error && error.status === 401) {
            this.authService.logout();
            console.log("Expired Credentials.");
        }

        return observableThrowError(error);
    }

}
