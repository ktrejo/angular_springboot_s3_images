import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

    constructor(private router: Router) {}

    canActivate() {
        //return true;
        
        if (sessionStorage.getItem('currentUser')) {
            return true;
        }

        //Not logged in
        this.router.navigate(['/login']);

        return false;
        
    }

    isAuthenticated() {
        if (sessionStorage.getItem('currentUser')) {
            return true;
        }

        return false;
    }
}
