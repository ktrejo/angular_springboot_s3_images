import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import { ImageService } from './image.service';

describe('ImageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ImageService]
    });
  });

  it('should be created', inject([HttpTestingController, ImageService], (httpMock: HttpTestingController, service: ImageService) => {
    expect(service).toBeTruthy();
  }));
});
