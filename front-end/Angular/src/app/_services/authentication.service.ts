import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

    api_url: string

    constructor(private http: HttpClient) {
        this.api_url='http://localhost:5000/api/';  
    }


    /**
    * register new user
    */
    register(username: string, password: string) {
        return this.authenticateUser(username, password, 'signup');     
    }


    /**
    * login user
    */
    login(username: string, password: string) {
        return this.authenticateUser(username, password, 'auth');
    }


    /**
    * Authenticate user
    */
    authenticateUser(username: string, password: string, url: string) {
        const data = {
            "username": username,
            "password": password
        };

        // when subscribe to, store the access token and refresh token in sessiong storage
        return this.http.post(this.api_url + url, data)
            .pipe(map(user => {
                if (user && user['token']) {
                    sessionStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user;
            }));
    }


    /**
    * Get token from an already login user.
    */
    getToken() {
        
        const user = sessionStorage.getItem('currentUser');

        if (user === '' || user === null)
            return "";

        return JSON.parse(user)['token'];
    }


    /**
    * Checks if user if signed in
    */
    userSignIn() {

        const user = sessionStorage.getItem('currentUser');

        if (user === null || user === '')
            return false;

        return true;
    }
    

    /**
    * Logout user
    */
    logout(){
        const user = sessionStorage.getItem('currentUser');

        // if logout is called before login
        if (user === null || user === '')
            return;

        sessionStorage.removeItem('currentUser');
    }

}
