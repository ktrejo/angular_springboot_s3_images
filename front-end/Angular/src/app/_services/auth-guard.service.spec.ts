import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BlankComponent, routes } from 'src/test/mock-route';

import { AuthGuardService } from './auth-guard.service';


describe('AuthGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        BlankComponent
      ],
      imports: [
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        AuthGuardService
      ]
    })
  });

  it('should be created', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    expect(service).toBeTruthy();
  });
});
