import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LightboxModule } from 'ngx-lightbox';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { ImageViewerComponent } from './image-viewer/image-viewer.component';
import { CreateAlbumTemplate, AddPhotoToAlbumTemplate, RemovePhotoAlbumTemplate, 
         DeleteAlbumTemplate, ShareAlbumTemplate } from './image-viewer/templates/templates';
import { UploadImageComponent } from './upload-image/upload-image.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';

import { TokenInterceptorService } from './_services/token-interceptor.service';
import { LoginComponent, TokenExpireModalContent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { BlankComponent } from 'src/test/mock-route';
import { UploadStatusFilterPipe } from './_pipes/upload-status-filter.pipe';
import { UploadCountFilterPipe } from './_pipes/upload-count-filter.pipe';
import { MockUploadCountFilter, MockUploadStatusFilter } from 'src/test/mock-pipe';

@NgModule({
  declarations: [
    BlankComponent,
    MockUploadCountFilter,
    MockUploadStatusFilter,
    AppComponent,
    ImageViewerComponent,
    CreateAlbumTemplate,
    AddPhotoToAlbumTemplate,
    RemovePhotoAlbumTemplate,
    DeleteAlbumTemplate,
    ShareAlbumTemplate,
    UploadImageComponent,
    HomeComponent,
    LoginComponent,
    TokenExpireModalContent,
    LogoutComponent,
    MockUploadCountFilter,
    MockUploadStatusFilter,
    UploadStatusFilterPipe,
    UploadCountFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    LightboxModule,
    NgbModule
  ],
  providers: [
    {
        provide: HTTP_INTERCEPTORS,
        useClass: TokenInterceptorService,
        multi: true
    },
    UploadStatusFilterPipe,
    UploadCountFilterPipe
  ],
  bootstrap: [AppComponent],
  entryComponents: [
      CreateAlbumTemplate,
      AddPhotoToAlbumTemplate,
      RemovePhotoAlbumTemplate,
      DeleteAlbumTemplate,
      ShareAlbumTemplate,
      TokenExpireModalContent
  ]
})
export class AppModule { }
