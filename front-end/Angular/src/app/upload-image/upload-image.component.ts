import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { ImageService } from '../_services/image.service';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { HttpEventType, HttpResponse, HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'app-upload-image',
    templateUrl: './upload-image.component.html',
    styleUrls: ['./upload-image.component.css']
})
export class UploadImageComponent implements AfterViewInit {

    @ViewChild('imageFileInput') imageInput: ElementRef;

    fileStatus: any[] = [];
    updateFilter: any[] = [];
    statusSections: String[] = [
      "Pending",
      "Uploading",
      "Done",
      "Failed"
    ];  
  
    constructor(private imageService: ImageService, private router: Router) { }
  
    ngAfterViewInit() {
      this.dragAndDrop()
      this.collapsible();
    }
  
    /**
     * Upload the file(s) the user submitted
     */
    upload() {
      let files: FileList = this.imageInput.nativeElement.files;
      for (let index = 0; index < files.length; index++) {
        let file = files[index];
        let entry = {
          name: file.name,
          file: file,
          percentage: 100,
          class: "dark",
          status: "Pending"
        }
        this.fileStatus.push(entry)
        this.uploadFile(entry);
      }
    }
  
  
    /**
     * Helper method for upload() function.
     * Keeps tracks of the upload progress for each image file the user submitted.
     * @param entry - an object containing the status report of the file to be uploaded.
     */
    uploadFile(entry) {
      this.imageService.uploadImage(entry.name, entry.file).pipe(
        map(event => {
          switch (event['type']) {
            case HttpEventType.UploadProgress:
              if (entry.status === "Pending") {
                entry.class = "primary";
                entry.status = "Uploading";
                // Triggers update on filter
                this.updateFilter = [];
              }
              entry.percentage = Math.round(event['loaded'] * 100 / event['total']);
              return event
  
            case HttpEventType.Response:
              return event;
          }
        })
      ).subscribe(data => {
        // Upload was successful
        if (data instanceof HttpResponse) {
          entry.class = "success";
          entry.status = "Done";
          this.updateFilter = [];
        }
        
      }, (err: HttpErrorResponse) => {
        // An error occurred during/after upload image
        entry.class = "danger";
        entry.status = "Failed";
        this.updateFilter = [];
  
        if (err && err.status === 403) {
          this.router.navigate(['/login'], { state: {token: 'expired'}});
        }
      })
    }
  
    /**
     * Add drag and drop highlight effects
     */
    dragAndDrop() {
      let area = document.getElementById("dropArea");
      let addClass = function(e) {
        this.classList.add("image-dropping");
      }
      let removeClass = function(e) {
        this.classList.remove("image-dropping");
      }
  
      area.addEventListener("dragenter", addClass);
      area.addEventListener("dragover", addClass);
      area.addEventListener("dragleave", removeClass);
      area.addEventListener("drop", removeClass);
    }
  
    /**
     * Add collapsible sections
     */
    collapsible() {
      let sections = document.getElementsByClassName("collapsible");
      
      for (let i = 0; i < sections.length; i++) {
        sections[i].addEventListener("click", function() {
          this.classList.toggle("active");
          this.nextElementSibling.classList.toggle("content-active");
        });
      }
    }
  }
  
