import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BlankComponent, routes } from 'src/test/mock-route';
import { ImageService } from '../_services/image.service';
import { mockImageService } from 'src/test/mock-image.service';

import { UploadImageComponent } from './upload-image.component';
import { MockUploadCountFilter, MockUploadStatusFilter } from 'src/test/mock-pipe';
import { NgbProgressbar } from '@ng-bootstrap/ng-bootstrap';

describe('UploadImageComponent', () => {
  let component: UploadImageComponent;
  let fixture: ComponentFixture<UploadImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        UploadImageComponent,
        BlankComponent,
        MockUploadCountFilter,
        MockUploadStatusFilter,
        NgbProgressbar
      ],
      imports: [
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        {
          provide: ImageService,
          useValue: mockImageService
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
