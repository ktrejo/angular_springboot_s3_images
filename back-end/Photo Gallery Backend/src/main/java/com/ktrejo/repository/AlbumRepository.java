package com.ktrejo.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ktrejo.model.Album;



@Repository
public interface AlbumRepository extends MongoRepository<Album, UUID> {
	
	public List<Album> findByOwner(String owner);

}
