package com.ktrejo.repository;

import java.util.UUID;

import com.ktrejo.model.Photo;

public interface UserPhotoCustomRepository {
	
	/**
	 * Atomic operation to insert a photo into the user document
	 * @param id - UUID of the user
	 * @param photo - Photo object to add the user's document
	 */
	public void insertPhoto(UUID id, Photo photo);

}
