package com.ktrejo.repository;

import java.util.UUID;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ktrejo.model.UserPhoto;


@Repository
public interface UserPhotoRepository extends MongoRepository<UserPhoto, UUID>, UserPhotoCustomRepository {
	
	public UserPhoto findByUsername(String username);
	
}
