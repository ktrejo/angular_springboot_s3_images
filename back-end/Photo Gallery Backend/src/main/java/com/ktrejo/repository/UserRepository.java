package com.ktrejo.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ktrejo.model.User;


@Repository
public interface UserRepository extends MongoRepository<User, String> {
 
    User findByUsername(String userName);
    
    User findByid(String id);
    
}
