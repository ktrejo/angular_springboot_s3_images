package com.ktrejo.repository;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.ktrejo.model.Photo;
import com.ktrejo.model.UserPhoto;

@Repository
public class UserPhotoCustomRepositoryImpl implements UserPhotoCustomRepository {
	
	@Autowired
	public MongoTemplate mongo;

	@Override
	public void insertPhoto(UUID id, Photo photo) {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(id));
		
		Update update = new Update();
		update.set("allPhotos." + photo.getId(), photo);
		
		mongo.findAndModify(query, update, UserPhoto.class);
	}
	
}
