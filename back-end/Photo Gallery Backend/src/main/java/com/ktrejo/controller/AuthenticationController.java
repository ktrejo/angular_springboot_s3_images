package com.ktrejo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ktrejo.dto.UserDTO;
import com.ktrejo.dto.TokenDTO;
import com.ktrejo.security.service.TokenService;
import com.ktrejo.service.ResponseGeneratorService;


@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

	@Autowired
	private TokenService tokenService;
    
	@Autowired
	private ResponseGeneratorService responseService;

    /**
     * Authenticate user through their provided username and password
     * @param dto - LoginDTO object. Contains username and password
     * @return A response object with jwt if authentication is successful or 
     * UNAUTHORIZED response
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> authenticate(@RequestBody UserDTO dto) {
        String token = tokenService.getToken(dto.getUsername().toLowerCase(), 
        										dto.getPassword());
        
        if (token != null) {
            TokenDTO response = new TokenDTO();
            response.setToken(token);
            
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
        	Object[][] body = new Object[][] {{"msg", "Authentication failed"}};
        	
        	return responseService.generateResponse(HttpStatus.UNAUTHORIZED, body);
        }
    }
}
