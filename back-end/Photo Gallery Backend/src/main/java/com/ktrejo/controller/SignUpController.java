package com.ktrejo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ktrejo.converter.ConverterFacade;
import com.ktrejo.dto.TokenDTO;
import com.ktrejo.dto.UserDTO;
import com.ktrejo.model.User;
import com.ktrejo.security.service.TokenService;
import com.ktrejo.service.ResponseGeneratorService;
import com.ktrejo.service.UserService;


@RestController
@RequestMapping("/api/signup")
public class SignUpController {

	@Autowired
	private UserService service;

	@Autowired
	private ConverterFacade converterFacade;
    
	@Autowired
	private TokenService tokenService;
    
	@Autowired
	private ResponseGeneratorService responseService;
    
    /**
     * Create a new user unless the user already exists
     * @param dto - UserDTO containing the username and password
     * @return A response containing jwt if new user signs up. Otherwise 
     * returns CONFLICT response
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> signUp(@RequestBody UserDTO dto) {
    	User user = service.create(converterFacade.convert(dto));
    	if (user != null) {
    		String token = tokenService.getToken(dto.getUsername(), dto.getPassword());
    		TokenDTO response = new TokenDTO();
            response.setToken(token);
            
            return new ResponseEntity<>(response, HttpStatus.OK);
            
    	} else {
    		return responseService.generateResponse(HttpStatus.CONFLICT, new Object[][] {
    			{"msg", "User already exist"}
    		});
    	}
        
    }
}
