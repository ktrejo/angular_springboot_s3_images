package com.ktrejo.controller;


import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ktrejo.model.Album;
import com.ktrejo.model.User;
import com.ktrejo.model.UserPhoto;
import com.ktrejo.service.AlbumService;
import com.ktrejo.service.ResponseGeneratorService;


@RestController
@RequestMapping("/api/test")
public class SecuredController {
	
	@Autowired
	private AlbumService albumService;
	
	@Autowired
	private ResponseGeneratorService responseService;
	
	
	/**
	 * A Test endpoint that returns all the details about a given user using
	 * their JSON web token
	 * @param request - HttpServletRequest object
	 * @return A response with all detail information about a user
	 */
	@RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> testController(HttpServletRequest request) {
    	User user = (User)request.getAttribute("User");
    	UserPhoto userPhoto = (UserPhoto)request.getAttribute("UserPhoto");
    	LinkedList<Album> AlbumList = new LinkedList<Album>();
    	userPhoto.getAlbums().forEach((albumSummary) -> {
    		AlbumList.add(albumService.getAlbum(albumSummary.getId()));
    	});
    	
    	return responseService.generateResponse(HttpStatus.OK, new Object[][] {
    		{"User", user},
    		{"UserPhoto", userPhoto},
    		{"Albums", AlbumList}
    	});
    	
    }
}
