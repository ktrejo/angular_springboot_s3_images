package com.ktrejo.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ktrejo.dto.PhotoListDTO;
import com.ktrejo.model.Album;
import com.ktrejo.model.UserPhoto;
import com.ktrejo.service.AlbumService;
import com.ktrejo.service.InternalHelperService;
import com.ktrejo.service.ResponseGeneratorService;

@RestController
public class AlbumController {
	
	@Autowired
	private AlbumService albumService;
	
	@Autowired
	private ResponseGeneratorService responseService;
	
	@Autowired
	private InternalHelperService internalService;
	
	
	/**
	 * Obtain information about the album from the database.
	 * @param albumId - RequestParam String. Id of album.
	 * @param request - HttpServletRequest object
	 * @return A response containing user's albumn information if successfull. Otherwise,
	 * returns a FORBIDDEN response
	 */
	@GetMapping("/api/user/album")
	public ResponseEntity<?> getAlbumInfo(@RequestParam String albumId, 
			HttpServletRequest request) {
		
		UserPhoto user = (UserPhoto)request.getAttribute("UserPhoto");
		Album album = albumService.getAlbum(internalService.StringToUUID(albumId));
		
		if (album == null) {
			return responseService.generateResponse(HttpStatus.BAD_REQUEST, NoAlbum());
		}
		
		// Only the owner and shared users can see info about the album
		if (!album.getOwner().equals(user.getUsername()) && 
				!album.getSharedUsers().contains(user.getUsername())) {
			
			return responseService.generateResponse(HttpStatus.FORBIDDEN, WrongOwner());
		}
		
		return responseService.generateResponse(HttpStatus.OK, SendAlbum(album));
	}
	
	
	/**
	 * Create a new private album for the user
	 * @param albumName - RequestParam String. Name of albumn
	 * @param request - HttpServletRequest object
	 * @return A response containing an updated UserPhoto information and 
	 * album information
	 */
	@PostMapping("/api/user/album")
	public ResponseEntity<?> createAlbum(@RequestParam String albumName,
			HttpServletRequest request) {
		
		UserPhoto user = (UserPhoto)request.getAttribute("UserPhoto");
		Album album = albumService.createAlbum(user, albumName);
				
		return responseService.generateResponse(HttpStatus.OK, SendBoth(user, album));
	}
	
	/**
	 * Deletes an album only if the user is the owner
	 * @param albumId - RequestParam String. Id of album to delete
	 * @param request - HttpServletRequest object
	 * @return A response containing updated UserPhoto information if successful.
	 * Otherwise a FORBIDDEN response
	 */
	@DeleteMapping("/api/user/album")
	public ResponseEntity<?> deleteAlbum(@RequestParam String albumId, 
			HttpServletRequest request) {
		
		UserPhoto user = (UserPhoto)request.getAttribute("UserPhoto");
		Album album = albumService.getAlbum(internalService.StringToUUID(albumId));
		
		if (album == null) {
			return responseService.generateResponse(HttpStatus.BAD_REQUEST, NoAlbum());
		}
		
		// Only the owner can delete the album
		if (!album.getOwner().equals(user.getUsername())) {
			return responseService.generateResponse(HttpStatus.FORBIDDEN, null);
		}
		
		this.albumService.deleteAlbum(user, album);
		
		return responseService.generateResponse(HttpStatus.OK, SendUserPhoto(user));
	}
	
	
	/**
	 * Add photo to private or shared album. The user must be the owner
	 * of the album to add a photo to the album.
	 * @param dto - PhotoListDTO object from request's body.
	 * @param albumId - RequestParam String. Id of album
	 * @param request - HttpServletRequest object
	 * @return A response containing an updated status of the album.
	 */
	@PostMapping("/api/user/album/update")
	public ResponseEntity<?> addPhotosToAlbum(@RequestBody PhotoListDTO dto,
			@RequestParam String albumId, HttpServletRequest request) {
		
		return updatePhotosToAlbum("ADD", dto, albumId, request);
	}
	
	/**
	 * Removes a photo from a private or shared album. The user must be the
	 * owner of the album.
	 * @param dto - PhotoListDTO object from request's body.
	 * @param albumId - RequestParam String. Id of album
	 * @param request - HttpServletRequest object
	 * @return A response containing updated status of the album.
	 */
	@DeleteMapping("/api/user/album/update")
	public ResponseEntity<?> removePhotosToAlbum(@RequestBody PhotoListDTO dto,
			@RequestParam String albumId, HttpServletRequest request) {
		
		return updatePhotosToAlbum("REMOVE", dto, albumId, request);
	}
	
	/**
	 * Allows a private album to be shared. This endpoint must be called first
	 * before allowing other users to access this album. The user must be the
	 * owner of the album to turn on sharing.
	 * @param albumId - RequestParam String. Id of album
	 * @param request - HttpServletRequest object
	 * @return A response containing updated status of the album.
	 */
	@PostMapping("/api/user/album/share")
	public ResponseEntity<?> turnShareOn(@RequestParam String albumId, 
			HttpServletRequest request) {
		
		UserPhoto user = (UserPhoto)request.getAttribute("UserPhoto");
		Album album = albumService.getAlbum(internalService.StringToUUID(albumId));
		
		if (album == null || !album.getOwner().equals(user.getUsername())) {
			return responseService.generateResponse(HttpStatus.FORBIDDEN, WrongOwner());
		}
		
		albumService.toggleShare(user, album);
		
		return responseService.generateResponse(HttpStatus.OK, SendAlbum(album));
	}
	
	/**
	 * Adds another user to a shared album. Must be owner of the album to use
	 * this endpoint
	 * @param albumId - RequestParam String. Id of album
	 * @param username - RequestParam String.
	 * @param request - HttpServletRequest object
	 * @return A response containing updated status of the album.
	 */
	@PostMapping("/api/user/album/share/update")
	public ResponseEntity<?> addUserToShareAlbum(@RequestParam String albumId,
			@RequestParam String username, HttpServletRequest request) {
		
		return updateUserToShareAlbum("ADD", albumId, username, request);
	}
	
	/**
	 * Removes a user from the shared album. Must be owner of the album to use
	 * this endpoint.
	 * @param albumId - RequestParam String. Id of album
	 * @param username - RequestParam String.
	 * @param request - HttpServletRequest object
	 * @return A response containing updated status of the album.
	 */
	@DeleteMapping("/api/user/album/share/update")
	public ResponseEntity<?> removeUserToShareAlbum(@RequestParam String albumId,
			@RequestParam String username, HttpServletRequest request) {
		
		return updateUserToShareAlbum("REMOVE", albumId, username, request);
	}
	

	/**
	 * Add or remove photo(s) from a given album
	 * @param command - String. Either "ADD" or "REMOVE"
	 * @param dto - PhotoListDTO
	 * @param albumId - String. Id of album
	 * @param request - HttpServletRequest object
	 * @return A response of an updated userPhoto and Album if successful. Otherwise
	 * returns a FORBIDDEN response
	 */
	private ResponseEntity<?> updatePhotosToAlbum(String command, 
			PhotoListDTO dto, String albumId, HttpServletRequest request) {
		
		UserPhoto user = (UserPhoto)request.getAttribute("UserPhoto");
		Album album = albumService.getAlbum(internalService.StringToUUID(albumId));
		
		if (album == null || !album.getOwner().equals(user.getUsername())) {
			return responseService.generateResponse(HttpStatus.FORBIDDEN, WrongOwner());
		}
		
		if (dto.getListOfIds() == null) {
			return responseService.generateResponse(HttpStatus.BAD_REQUEST, ErrorInDTO());
		}
		
		// Avoid duplicate keys
		List<UUID> list = new ArrayList<UUID>(dto.getListOfIds().size());
		Set<String> set = new HashSet<String>();
		
		for (String str : dto.getListOfIds()) {
			UUID uuid = internalService.StringToUUID(str);
			if (uuid != null && !set.contains(str)) {
				list.add(UUID.fromString(str));
				set.add(str);
			}
		}
		
		switch(command) {
			case "ADD":
				albumService.addPhotos(user, list, album);
				break;
			case "REMOVE":
				albumService.removePhotos(user, list, album);
				break;
			default:
				break;
		}
		
		return responseService.generateResponse(HttpStatus.OK, SendBoth(user, album));
	}
	

	/**
	 * Add or Remove a person from the shared album
	 * @param command - String. Either "ADD" or "REMOVE"
	 * @param albumId - String. Id of album
	 * @param username - String. Username of the other person.
	 * @param request - HttpServletRequest object
	 * @return
	 */
	private ResponseEntity<?> updateUserToShareAlbum(String command, 
			String albumId, String username, HttpServletRequest request) {
		
		UserPhoto user = (UserPhoto)request.getAttribute("UserPhoto");
		Album album = albumService.getAlbum(internalService.StringToUUID(albumId));
		
		if (album == null) {
			return responseService.generateResponse(HttpStatus.BAD_REQUEST, NoAlbum());
		}
		
		if (album.isSharedTurnOn() == false) {
			return responseService.generateResponse(HttpStatus.BAD_REQUEST, ShareOff());
		}
		
		if (!(user.getUsername().equals(album.getOwner()))) {
			return responseService.generateResponse(HttpStatus.FORBIDDEN, WrongOwner());
		}
		
		// Do not update shared album if username is the same as the owner
		if (!user.getUsername().equals(username)) {
			switch(command) {
				case "ADD":
					albumService.addUserToSharedAlbum(user, username, album);
					break;
				case "REMOVE":
					albumService.removeUserFromSharedAlbum(user, username, album);
					break;
				default:
					break;
			}
		}
		
		return responseService.generateResponse(HttpStatus.OK, SendAlbum(album));
	}
	
	private Object[][] WrongOwner() {
		return new Object[][] {
			{"Msg", "User Does Not Own Album."}
		};
	}
	
	private Object[][] NoAlbum() {
		return new Object[][] {
			{"Msg", "Album Does Not Exist"}
		};
	}
	
	private Object[][] ShareOff() {
		return new Object[][] {
			{"Msg", "Sharing feature for this album is disabled."}
		};
	}
	
	private Object[][] ErrorInDTO() {
		return new Object[][] {
			{"Msg", "listOfIds field is not found."}
		};
	}
	
	private Object[][] SendAlbum(Album album) {
		return new Object[][] {
			{"Album", album}
		};
	}
	
	private Object[][] SendUserPhoto(UserPhoto userPhoto) {
		return new Object[][] {
			{"UserPhoto", userPhoto}
		};
	}
	
	private Object[][] SendBoth(UserPhoto userPhoto, Album album) {
		return new Object[][] {
			{"UserPhoto", userPhoto},
			{"Album", album}
		};
	}
}
