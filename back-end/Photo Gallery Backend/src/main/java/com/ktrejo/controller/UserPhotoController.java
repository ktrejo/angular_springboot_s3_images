package com.ktrejo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ktrejo.dto.PhotoDTO;
import com.ktrejo.dto.PhotoListDTO;
import com.ktrejo.model.UserPhoto;
import com.ktrejo.service.InternalHelperService;
import com.ktrejo.service.PhotoService;
import com.ktrejo.service.ResponseGeneratorService;

@RestController
public class UserPhotoController {
	
	@Autowired
	private PhotoService photoService;
	
	@Autowired
	private ResponseGeneratorService responseService;
	
	@Autowired
	private InternalHelperService internalService;
		

	/**
	 * Get information about the user's photo gallery
	 * @param request
	 * @return a UserPhoto object in the response
	 */
	@GetMapping("/api/user/photo")
    public ResponseEntity<?> userPhoto(HttpServletRequest request) {
		
    	UserPhoto userPhoto = (UserPhoto)request.getAttribute("UserPhoto");
    	
    	return responseService.generateResponse(HttpStatus.OK, SendUserPhoto(userPhoto));
    }
	
	
	/**
	 * Add a photo to the user's photo gallery
	 * @param dto
	 * @param request
	 * @return an updated UserPhoto object in the response
	 */
	@PostMapping("/api/user/photo")
	public ResponseEntity<?> insertUserPhoto(@ModelAttribute PhotoDTO dto,
			HttpServletRequest request) {
		
		UserPhoto userPhoto = (UserPhoto)request.getAttribute("UserPhoto");
		photoService.addPhoto(userPhoto, dto.getKeyName(), dto.getUploadFile());
		
		return responseService.generateResponse(HttpStatus.OK, SendUserPhoto(userPhoto));
	}
	
	
	/**
	 * Remove photos (single or multiple) from the user's photo gallery
	 * @param dto
	 * @param request
	 * @return an updated UserPhoto object in the response
	 */
	@DeleteMapping("/api/user/photo")
	public ResponseEntity<?> removeUserPhoto(@RequestBody PhotoListDTO dto, 
			HttpServletRequest request) {
		
		UserPhoto userPhoto = (UserPhoto)request.getAttribute("UserPhoto");
		List<UUID> idsToDelete = new ArrayList<UUID>(dto.getListOfIds().size());
		Map<String, Object> map = new HashMap<String, Object>();
		
		dto.getListOfIds().forEach(strId -> {
			UUID uuid = internalService.StringToUUID(strId);
			if (uuid != null) {
				idsToDelete.add(uuid);
			}
		});
		
		photoService.deleteMultiPhoto(userPhoto, idsToDelete);
    	map.put("UserPhoto", userPhoto);
		
    	return responseService.generateResponse(HttpStatus.OK, SendUserPhoto(userPhoto));
	}
	
	private Object[][] SendUserPhoto(UserPhoto userPhoto) {
		return new Object[][] {
			{"UserPhoto", userPhoto}
		};
	}
}
