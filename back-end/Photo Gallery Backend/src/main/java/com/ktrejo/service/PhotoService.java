package com.ktrejo.service;

import java.util.List;
import java.util.UUID;

import org.springframework.web.multipart.MultipartFile;

import com.ktrejo.model.User;
import com.ktrejo.model.UserPhoto;

public interface PhotoService {
	
	/**
	 * Create a new UserPhoto object. Set username in photo to user.username
	 * @param user -User Object
	 * @return UserPhoto object for User user
	 */
	public UserPhoto createUserPhoto(User user);
	

	/**
	 * Add photo to UserPhoto
	 * @param userPhoto - UserPhoto object
	 * @param photoName - Name of photo
	 * @param file - photo's data in MultipartFile object
	 * @return Updated UserPhoto object
	 */
	public UserPhoto addPhoto(UserPhoto userPhoto, String photoName, 
			MultipartFile file);
	
	/**
	 * Delete Photo from UserPhoto
	 * @param userPhoto - UserPhoto object
	 * @param key - String UUID of photo
	 * @return Updated UserPhoto object
	 */
	public UserPhoto deletePhoto(UserPhoto userPhoto, UUID key);
	

	/**
	 * Delete all photos from UserPhoto found in keys
	 * @param userPhoto - UserPhoto object
	 * @param keys - List of UUID Strings
	 * @return Updated UserPhoto object
	 */
	public UserPhoto deleteMultiPhoto(UserPhoto userPhoto, 
										List<UUID> keys);

}
