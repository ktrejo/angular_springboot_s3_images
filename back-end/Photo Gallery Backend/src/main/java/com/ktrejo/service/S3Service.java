package com.ktrejo.service;

import java.io.ByteArrayOutputStream;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public interface S3Service {
	
	/**
	 * 
	 * @param key - url String key for the image.
	 * @return
	 */
	public ByteArrayOutputStream downloadImageFile(String key);
	
	/**
	 * Upload and image to s3 bucket.
	 * @param key - url String key for image destination
	 * @param tags - 2D String array that contains custom meta data
	 * @param file - MultipartFile object containing the image data.
	 * @return true if successful. False Otherwise.
	 */
	public boolean uploadImageFile(String key, String[][] tags, MultipartFile file);

	/**
	 * Copies image file name as srcName to image file as destName
	 * @param srcName - url string. 
	 * @param destName - url string.
	 * @return true if successful. False Otherwise.
	 */
	public boolean copyImageFile(String srcName, String destName);
	
	/**
	 * Delete an image with the same keyName.
	 * @param key - url to delete image
	 * @return true if successful. False Otherwise.
	 */
	public boolean deleteImageFile(String key);
	
	/**
	 * Delete multiple images from listOfNames.
	 * @param listOfNames 
	 * @return true if successful. Returns false otherwise.
	 */
	public boolean deleteMultiImage(List<String> listOfNames);
	
	/**
	 * 
	 * @return all keys currently in the s3 bucket.
	 */
	public List<String> listImageFileNames();

}
