package com.ktrejo.service;

import java.util.List;
import java.util.UUID;

import com.ktrejo.model.Album;
import com.ktrejo.model.UserPhoto;

public interface AlbumService {
	
	/**
	 * Create album and assign ownership to user.
	 * @param user - UserPhoto object
	 * @param albumName - String object. Name of albumn.
	 * @return Album if successful. Null otherwise.
	 */
	public Album createAlbum(UserPhoto user, String albumName);
	
	/**
	 * Delete an album only if the owner of the album is the user.
	 * @param user - UserPhoto object
	 * @param album - Album Object
	 * @return true if successful. Returns false otherwise.
	 */
	public boolean deleteAlbum(UserPhoto user, Album album);
	
	/**
	 * Get Album from database.
	 * @param albumId
	 * @return Album if present in database. Null otherwise.
	 */
	public Album getAlbum(UUID albumId);
	
	/**
	 * Set album to share with other users
	 * @param user - UserPhoto object. User that owns albumn.
	 * @param album - Album object.
	 * @return Updated Album object.
	 */
	public Album toggleShare(UserPhoto user, Album album);
	
	/**
	 * Add list of photos to an album.
	 * @param user - UserPhoto object. Owner of album.
	 * @param photoIds - List of UUID. Photo ids to add to an album.
	 * @param album - Album object.
	 * @return True if successful. False Otherwise.
	 */
	public boolean addPhotos(UserPhoto user, 
			List<UUID> photoIds, Album album);
	
	/**
	 * Remove list of photos from an album.
	 * @param user - UserPhoto object. Owner of album.
	 * @param photoIds - List of UUID. Photo ids to remove from the album.
	 * @param album - Album object.
	 * @return True if successful. False otherwise.
	 */
	public boolean removePhotos(UserPhoto user, 
			List<UUID> photoIds, Album album);
	
	/**
	 * Add User from shared album.
	 * @param user - UserPhoto object. Owner of album.
	 * @param username - String object. Username to add to albumn.
	 * @param album - Album object.
	 * @return Updated album object.
	 */
	public Album addUserToSharedAlbum(UserPhoto user, 
			String username, Album album);
	
	/**
	 * Remove User from shared album.
	 * @param user - UserPhoto object. Owner of album.
	 * @param username - String object. Username to remove from albumn.
	 * @param album - Album object.
	 * @return Updated album object.
	 */
	public Album removeUserFromSharedAlbum(UserPhoto user, 
			String username, Album album);

}
