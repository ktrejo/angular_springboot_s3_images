package com.ktrejo.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public interface ResponseGeneratorService {
	
	/**
	 * Creates Response with JSON message
	 * @param status - HttpStatus
	 * @param objects - variable arguments. Pairs of object to add in response body
	 * @return ResponseEntity
	 */
	public ResponseEntity<?> generateResponse(HttpStatus status, Object[][] body);

}
