package com.ktrejo.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
 
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest.KeyVersion;


/**
 * @author kevin
 *
 */
@Service
public class S3ServiceImpl implements S3Service {
	
	private Logger logger = LoggerFactory.getLogger(S3ServiceImpl.class);
	
	@Autowired
	private AmazonS3 s3client;
 
	@Value("${s3.bucket}")
	private String bucketName;
 
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ByteArrayOutputStream downloadImageFile(String key) {
		
		if (key == null) {
			return null;
		}
		
		try {
			
            S3Object s3object = s3client.getObject(new GetObjectRequest(bucketName, key));
            
            InputStream is = s3object.getObjectContent();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int len;
            byte[] buffer = new byte[4096];
            
            while ((len = is.read(buffer, 0, buffer.length)) != -1) {
                baos.write(buffer, 0, len);
            }
            
            return baos;
            
		} catch (IOException ioe) {
			logIOException(ioe);
			
        } catch (AmazonServiceException ase) {
        	logAmazonServiceException(ase);
			
			throw ase;
			
        } catch (AmazonClientException ace) {
        	logAmazonClientException(ace);
            
            throw ace;
        }
		
		return null;
	}
 
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean uploadImageFile(String key, String[][] tags, MultipartFile file) {
		
		if (key == null || tags == null || file == null) {
			return false;
		}
		
		try {
			
			String imageType = file.getContentType();
			
			// file is not an image.
			if (imageType == null) {
				return false;
			}
			
			ObjectMetadata metadata = new ObjectMetadata();
			
			metadata.setContentLength(file.getSize());
			metadata.setContentType(imageType);
			for (String[] metaTag : tags) {
				metadata.addUserMetadata(metaTag[0], metaTag[1]);
			}
			
			s3client.putObject(bucketName, key, file.getInputStream(), metadata);
			
			return true;
			
		} catch(IOException ioe) {
			logIOException(ioe);
			
			return false;
			
		} catch (AmazonServiceException ase) {
			logAmazonServiceException(ase);
			
			return false;
			
        } catch (AmazonClientException ace) {
        	logAmazonClientException(ace);
            
            return false;
            
        }
		
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean copyImageFile(String srcName, String destName) {
		
		if (srcName == null || destName == null) {
			return false;
		}
		
		try {
			CopyObjectRequest request = new CopyObjectRequest(this.bucketName,
					srcName, bucketName, destName);
			
			s3client.copyObject(request);
			
			return true;
			
		} catch(AmazonServiceException ase) {
			logAmazonServiceException(ase);
			
			return false;
			
		} catch (SdkClientException sce) {
			logSDKClientException(sce);
            
            return false;
		}
		
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean deleteImageFile(String key) {
		
		if (key == null) {
			return false;
		}
		
		try {
			s3client.deleteObject(new DeleteObjectRequest(bucketName, key));
			
			return true;
			
		} catch (AmazonServiceException ase) {
			logAmazonServiceException(ase);
            
            return false;
			
		} catch (SdkClientException sce) {
			logSDKClientException(sce);
            
            return false;
		
		}
		
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean deleteMultiImage(List<String> listOfNames) {
		
		if (listOfNames == null) {
			return false;
		}
		
		try {
			
			List<KeyVersion> keyNames = new ArrayList<KeyVersion>(listOfNames.size());
			listOfNames.forEach((name) -> keyNames.add(new KeyVersion(name)));
			
			DeleteObjectsRequest s3req = new DeleteObjectsRequest(bucketName);
			s3req.withKeys(keyNames);
			
			s3client.deleteObjects(s3req);
			
			return true;
			
		} catch (AmazonServiceException ase) {
			logAmazonServiceException(ase);
            
            return false;
			
		} catch (SdkClientException sce) {
			logSDKClientException(sce);
            
            return false;
		
		}
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> listImageFileNames() {
		
		try {
			
			List<String> listOfKeys;
			List<S3ObjectSummary> listOfSummaries = new ArrayList<S3ObjectSummary>(25);
			ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucketName).withMaxKeys(25);
			ListObjectsV2Result result;
			
			do {
				
				result = s3client.listObjectsV2(req);
				
				for (S3ObjectSummary objectSummary : result.getObjectSummaries()) {
					listOfSummaries.add(objectSummary);
				}
				
				// If there are more than maxKeys keys in the bucket, get a continuation token
                // and list the next objects.
                String token = result.getNextContinuationToken();
                req.setContinuationToken(token);
				
			} while (result.isTruncated());
			
			listOfKeys = new ArrayList<String>(listOfSummaries.size());
			listOfSummaries.sort(new Comparator<S3ObjectSummary>() {

				public int compare(S3ObjectSummary one, S3ObjectSummary two) {
					return two.getLastModified().compareTo(one.getLastModified());
				}
				
			});
			
			for (S3ObjectSummary objectSummary : listOfSummaries) {
				listOfKeys.add(objectSummary.getKey());
			}
			
			
			return listOfKeys;
			
		} catch (AmazonServiceException ase) {
			logAmazonServiceException(ase);
            
            throw ase;
			
		} catch (SdkClientException sce) {
			logSDKClientException(sce);
            
            throw sce;
		
		}
		
	}

	
	private void logIOException(IOException ioe) {
		logger.error("IOException: " + ioe.getMessage());
	}
	
	
	private void logAmazonServiceException(AmazonServiceException ase) {
		
		// The call was transmitted successfully, but Amazon S3 couldn't process 
        // it, so it returned an error response.
		logger.info("Caught an AmazonServiceException from GET requests, rejected reasons:");
		logger.info("Error Message:    " + ase.getMessage());
		logger.info("HTTP Status Code: " + ase.getStatusCode());
		logger.info("AWS Error Code:   " + ase.getErrorCode());
		logger.info("Error Type:       " + ase.getErrorType());
		logger.info("Request ID:       " + ase.getRequestId());
		
	}
	
	
	private void logAmazonClientException(AmazonClientException ace) {
		
		logger.info("Caught an AmazonClientException: ");
        logger.info("Error Message: " + ace.getMessage());
        
	}
		
        
	private void logSDKClientException(SdkClientException sce) {
		
		// Amazon S3 couldn't be contacted for a response, or the client
        // couldn't parse the response from Amazon S3.
		logger.info("Caught an SdkClientException: ");
        logger.info("Error Message: " + sce.getMessage());
        
	}

		
}