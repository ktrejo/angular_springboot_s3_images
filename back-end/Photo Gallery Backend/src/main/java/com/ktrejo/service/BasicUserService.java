package com.ktrejo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ktrejo.model.User;
import com.ktrejo.model.UserPhoto;
import com.ktrejo.repository.UserPhotoRepository;
import com.ktrejo.repository.UserRepository;

import java.time.Instant;
import java.util.List;


@Service
public class BasicUserService implements UserService {

	@Autowired
	private UserRepository repository;
	
	@Autowired
	private UserPhotoRepository photoRepository;
	
	@Autowired
	private PhotoService photoService;
    
    /*
     * Create a new user if possible. If new user is created, then the new user
     * is tied to a newly created UserPhoto object. Returns new user. Returns
     * null if service fails to create a new user
     */
    @Override
    public User create(User user) {
    	if (repository.findByUsername(user.getUsername()) == null) {
    		
    		UserPhoto userPhoto = photoService.createUserPhoto(user);
    		photoRepository.save(userPhoto);
    			
    		user.setUserPhotoId(userPhoto.getId());
    		user.setCreatedAt(String.valueOf(Instant.now()));
    		
            return repository.save(user);
            
    	} else {
    		return null;
    	}
        
    }

    @Override
    public User find(String id) {
    	return repository.findByid(id);
    }

    @Override
    public User findByUsername(String userName) {
        return repository.findByUsername(userName);
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public User update(String id, User user) {
        user.setId(id);

        User saved = repository.findByid(id);

        if (saved != null) {
            user.setCreatedAt(saved.getCreatedAt());
            user.setUpdatedAt(String.valueOf(Instant.now()));
        } else {
            user.setCreatedAt(String.valueOf(Instant.now()));
        }
        repository.save(user);
        return user;
    }

    @Override
    public String delete(String id) {
        repository.deleteById(id);
        return id;
    }
}
