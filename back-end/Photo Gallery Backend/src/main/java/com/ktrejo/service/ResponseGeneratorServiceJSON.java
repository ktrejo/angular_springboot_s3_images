package com.ktrejo.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ResponseGeneratorServiceJSON implements ResponseGeneratorService {
	
	Logger logger = LoggerFactory.getLogger(ResponseGeneratorServiceJSON.class);

	/**
	 * Creates Response with JSON message
	 * @param status - HttpStatus
	 * @param objects - variable arguments. Pairs of object to add in response body
	 * @return ResponseEntity
	 */
	@Override
	public ResponseEntity<?> generateResponse(HttpStatus status, Object[][] body) {
		Map<Object, Object> map = new HashMap<Object, Object>();
		String logMsg = "";
		
		if (status == null) {
			logMsg = "Error! HttpStatus code is null";
			return InternalServerErrorResponse(logMsg);
		}
		
		if (body != null) {
			for (Object[] pair : body) {
				if (pair.length != 2) {
					logMsg = "Error! Length of String array is not 2. Must be in 2 to "
							+ "create json mapping";
					return InternalServerErrorResponse(logMsg);
				}
				
				map.put(pair[0], pair[1]);
			}
		}
		
		return new ResponseEntity<>(map, status);
	}
	
	private ResponseEntity<?> InternalServerErrorResponse(String logMsg) {
		logger.error(logMsg);
		
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
