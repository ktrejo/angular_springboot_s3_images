package com.ktrejo.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ktrejo.model.AlbumSummary;
import com.ktrejo.model.Photo;
import com.ktrejo.model.User;
import com.ktrejo.model.UserPhoto;
import com.ktrejo.repository.UserPhotoRepository;

@Service
public class PhotoServiceImpl implements PhotoService {
	
	@Autowired
	private S3Service s3Service;
	
	@Autowired
	private UserPhotoRepository photoRepository;
	
	@Autowired
	private AlbumService albumService;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserPhoto createUserPhoto(User user) {
		
		if (user == null) {
			return null;
		}
		
		UserPhoto userPhoto = new UserPhoto();
		userPhoto.setId(UUID.randomUUID());
		userPhoto.setUsername(user.getUsername());
		userPhoto.setFolderName(userPhoto.getId().toString());
		userPhoto.setAllPhotos(new HashMap<UUID, Photo>());
		userPhoto.setAlbums(new HashSet<AlbumSummary>());
		
		return userPhoto;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserPhoto addPhoto(UserPhoto userPhoto, String photoName, 
			MultipartFile file) {
		
		if (userPhoto == null || photoName == null || file == null) {
			return userPhoto;
		}
		
		String[] arr = photoName.split("\\.");
		if (arr.length <= 1) {
			return userPhoto;
		}
		
		
		String fileType = "." + arr[arr.length-1];
		// Create photo object
		Photo photo = new Photo(photoName, fileType, userPhoto.getUsername());
		String s3Key = userPhoto.getFolderName() + "/" + photo.getUrlId();
		String[][] tags = new String[][] {{"Title", photoName}};
		
		// Update UserPhoto object only if upload to S3 storage if successful.
		if (s3Service.uploadImageFile(s3Key, tags, file)) {
			userPhoto.getAllPhotos().put(photo.getId(), photo);
			photoRepository.insertPhoto(userPhoto.getId(), photo);
		}
		
		return userPhoto;	
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserPhoto deletePhoto(UserPhoto userPhoto, UUID key) {
		
		if (userPhoto == null || key == null) {
			return userPhoto;
		}
		
		Photo photoToDelete = userPhoto.getAllPhotos().get(key);
		
		if (photoToDelete == null) {
			return userPhoto;
		}
		
		String folderPath = userPhoto.getFolderName() + "/";
		String s3KeyName = folderPath + photoToDelete.getUrlId();
		
		// Remove photo from UserPhoto object if deletion from S3 storage
		// is successful
		if (s3Service.deleteImageFile(s3KeyName)) {
			
			Set<AlbumSummary> albums = userPhoto.getAlbums();
			List<UUID> photoIdList = new LinkedList<UUID>();
			photoIdList.add(key);
			
			// Delete the photo from every album containing this photo.
			// As long as the owner of the album is this user.
			albums.forEach(album ->
				this.albumService.removePhotos(userPhoto, photoIdList, 
					this.albumService.getAlbum(album.getId())));
			
			userPhoto.getAllPhotos().remove(key);
			
			photoRepository.save(userPhoto);
		}
		
		return userPhoto;	
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserPhoto deleteMultiPhoto(UserPhoto userPhoto, 
										List<UUID> keys) {
		
		if (userPhoto == null || keys == null) {
			return userPhoto;
		}
		
		String folderPath = userPhoto.getFolderName() + "/";
		Map<UUID, Photo> allPhotos = userPhoto.getAllPhotos();
		List<String> s3ListOfNames = new LinkedList<String>();
		
		keys.forEach((key) -> {
			Photo keyForPhoto = allPhotos.get(key);
			if (keyForPhoto != null) {
				s3ListOfNames.add(folderPath + keyForPhoto.getUrlId());
			}
		});
		
		// Remove all photos from UserPhoto object if deletion from S3 storage
		// is successful
		if (s3Service.deleteMultiImage(s3ListOfNames)) {
			
			Set<AlbumSummary> albums = userPhoto.getAlbums();
			
			// Delete the photo from every album containing this photo.
			// As long as the owner of the album is this user.
			albums.forEach(album -> 
				this.albumService.removePhotos(userPhoto, keys, 
						this.albumService.getAlbum(album.getId())));
			
			keys.forEach((key) -> {
				userPhoto.getAllPhotos().remove(key);
			});
			
			photoRepository.save(userPhoto);
		}
		
		return userPhoto;
	}

}
