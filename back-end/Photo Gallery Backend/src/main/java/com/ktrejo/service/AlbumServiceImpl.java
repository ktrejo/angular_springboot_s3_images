package com.ktrejo.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ktrejo.model.Album;
import com.ktrejo.model.AlbumSummary;
import com.ktrejo.model.Photo;
import com.ktrejo.model.UserPhoto;
import com.ktrejo.repository.AlbumRepository;
import com.ktrejo.repository.UserPhotoRepository;

@Service
public class AlbumServiceImpl implements AlbumService {
	
	@Autowired
	private S3Service s3Service;
	
	@Autowired
	private AlbumRepository albumRepository;
	
	@Autowired
	private UserPhotoRepository userPhotoRepository;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Album createAlbum(UserPhoto user, String albumName) {
		
		HashSet<String> sharedUsers = new HashSet<String>();
		HashMap<UUID, Photo> allPhotos = new HashMap<UUID, Photo>();
		Album album = new Album(user, albumName, sharedUsers, allPhotos);
		albumRepository.save(album);
		
		AlbumSummary newSummary = new AlbumSummary(album);
		user.getAlbums().add(newSummary);
		userPhotoRepository.save(user);
		
		return album;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean deleteAlbum(UserPhoto user, Album album) {
		
		// Shared album has a separate copy of the photos stored in the album
		// Must delete the separate copy first.
		if (album.isSharedTurnOn()) {
			String path = "album/" + album.getFolderName() + "/";
			album.getAllPhotos().forEach((id, photo) -> {
				s3Service.deleteImageFile(path + photo.getUrlId());
			});
		}
		AlbumSummary albumSummaryToRemove = new AlbumSummary(album);
		List<UserPhoto> userPhotoToUpdate = new LinkedList<UserPhoto>();
		user.getAlbums().remove(albumSummaryToRemove);
		userPhotoToUpdate.add(user);
		
		List<UserPhoto> sharedUsers = new LinkedList<UserPhoto>();
		album.getSharedUsers().forEach(sharedUsername -> {
			sharedUsers.add(userPhotoRepository.findByUsername(sharedUsername));
		});
		sharedUsers.forEach(otherUser -> {
			System.out.println(otherUser.getUsername());
			otherUser.getAlbums().remove(albumSummaryToRemove);
			userPhotoToUpdate.add(otherUser);
		});
		
		albumRepository.delete(album);
		userPhotoRepository.saveAll(userPhotoToUpdate);
		
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Album getAlbum(UUID albumId) {
		if (albumId != null && albumRepository.existsById(albumId)) {
			return albumRepository.findById(albumId).get();
		}
		
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Album toggleShare(UserPhoto user, Album album) {
		if (!album.isSharedTurnOn()) {
			album.setSharedTurnOn(true);
			album.setFolderName(album.getId().toString());
			
			List<UUID> list = new LinkedList<UUID>();
			list.addAll(album.getAllPhotos().keySet());
			// S3 service was able to copy all photos to a different location
			// Thus, save new changes to album
			if (addPhotos(user, list, album)) {
				albumRepository.save(album);
			}
			
		} 
		
		return album;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addPhotos(UserPhoto user, List<UUID> photoIds, Album album) {
		
		// Only allow update photo if user is owner of the album
		if (!user.getUsername().equals(album.getOwner())) {
			return false;
		}
		
		boolean shared = album.isSharedTurnOn();
		photoIds.forEach((id) -> {
			Photo photoFromId = user.getAllPhotos().get(id);
			if (photoFromId != null) {
				album.getAllPhotos().put(id, photoFromId);
				// create a shared copy of the photo if album is shared
				if (shared) {
					String urlId = photoFromId.getUrlId();
					String srcPath = user.getFolderName() + "/" + urlId;
					String destPath = "album/" + album.getFolderName() + "/" + urlId;
					
					s3Service.copyImageFile(srcPath, destPath);
				}
			}
		});
		
		albumRepository.save(album);
		
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removePhotos(UserPhoto user, List<UUID> photoIds, Album album) {
		
		// Only allow update photo if user is owner of the album
		if (!user.getUsername().equals(album.getOwner())) {
			return false;
		}
		
		boolean shared = album.isSharedTurnOn();
		
		photoIds.forEach((id) -> {
			if (user.getAllPhotos().containsKey(id)) {
				Photo photoToDelete = album.getAllPhotos().remove(id);
				// Remove shared copy if album is shared
				if (shared) {
					String path = "album/" + album.getFolderName() + 
							  	  "/" + photoToDelete.getUrlId();
					
					s3Service.deleteImageFile(path);
				}
			}
		});
		
		albumRepository.save(album);
		
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Album addUserToSharedAlbum(UserPhoto user, String username, 
			Album album) {
		
		if (album.isSharedTurnOn()) {
			UserPhoto otherUser = userPhotoRepository.findByUsername(username);
			if (otherUser != null && 
				!user.getUsername().equals(otherUser.getUsername())) {
				
				album.getSharedUsers().add(username);
				albumRepository.save(album);
				
				otherUser.getAlbums().add(new AlbumSummary(album));
				userPhotoRepository.save(otherUser);
			}
		}
		
		return album;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Album removeUserFromSharedAlbum(UserPhoto user, String username, 
			Album album) {

		if (album.isSharedTurnOn()) {
			UserPhoto otherUser = userPhotoRepository.findByUsername(username);
			if (otherUser != null && 
				!user.getUsername().equals(otherUser.getUsername())) {
				
				album.getSharedUsers().remove(username);
				albumRepository.save(album);
				
				otherUser.getAlbums().remove(new AlbumSummary(album));
				userPhotoRepository.save(otherUser);
			}
		}
		
		return album;
	}

}
