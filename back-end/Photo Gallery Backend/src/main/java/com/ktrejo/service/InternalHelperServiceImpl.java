package com.ktrejo.service;

import java.util.UUID;

import org.springframework.stereotype.Service;

@Service
public class InternalHelperServiceImpl implements InternalHelperService {

	@Override
	public UUID StringToUUID(String string) {
		if (string == null) {
			return null;
		}
		
		try {
			return UUID.fromString(string);
			
		} catch(IllegalArgumentException e) {
			return null;
		}
	}

}
