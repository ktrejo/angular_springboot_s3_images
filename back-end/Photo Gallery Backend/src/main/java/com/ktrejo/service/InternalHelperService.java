package com.ktrejo.service;

import java.util.UUID;

public interface InternalHelperService {
	
	/**
	 * 
	 * @param string - a string that's formatted in UUID
	 * @return a UUID if string is valid. Otherwise null
	 */
	public UUID StringToUUID(String string);

}
