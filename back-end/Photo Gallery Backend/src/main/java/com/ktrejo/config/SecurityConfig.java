package com.ktrejo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.ktrejo.repository.UserPhotoRepository;
import com.ktrejo.security.RestAuthenticationEntryPoint;
import com.ktrejo.security.filter.AuthenticationTokenFilter;
import com.ktrejo.security.filter.CorsFilter;
import com.ktrejo.security.filter.UserPhotoFilter;
import com.ktrejo.security.service.TokenAuthenticationService;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private TokenAuthenticationService tokenAuthenticationService;
	private UserPhotoRepository photoRepository;

	@Autowired
	protected SecurityConfig(TokenAuthenticationService tokenAuthenticationService,
			UserPhotoRepository photoRepository) {
		super();
		this.tokenAuthenticationService = tokenAuthenticationService;
		this.photoRepository = photoRepository;
	}

	/**
	 * Configures Filters for http requests
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/api/auth").permitAll()
			.antMatchers("/api/signup").permitAll()
			.anyRequest().authenticated().and()
			.addFilterBefore(new AuthenticationTokenFilter(tokenAuthenticationService),
						UsernamePasswordAuthenticationFilter.class)
			.addFilterBefore(new CorsFilter(), AuthenticationTokenFilter.class)
			.addFilterAfter(new UserPhotoFilter(photoRepository), 
					UsernamePasswordAuthenticationFilter.class)
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			.csrf().disable().exceptionHandling()
			.authenticationEntryPoint(new RestAuthenticationEntryPoint());
	}
}
