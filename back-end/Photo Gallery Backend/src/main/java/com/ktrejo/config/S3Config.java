package com.ktrejo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

@Configuration
public class S3Config {

	@Autowired
	private AWSConfig awsConfig;

	/**
	 *  insert region from application.properties
	 */
	@Value("${s3.region}")
	private String region;

	/**
	 * Builds and returns an s3client.
	 */
	@Bean
	public AmazonS3 s3client() {

		BasicAWSCredentials awsCreds = awsConfig.basicAWSCredentials();
		// Bullds an s3Client using the user credentials and s3 bucket region.
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.fromName(region))
				.withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();

		return s3Client;
	}
}
