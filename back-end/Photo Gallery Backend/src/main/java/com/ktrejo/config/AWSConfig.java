package com.ktrejo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.BasicAWSCredentials;

@Configuration
public class AWSConfig {

	/**
	 * Insert access key from application.properties
	 */
	@Value("${aws.access_key_id}")
	private String awsId;

	/**
	 * Insert secret access key from application.properties
	 */
	@Value("${aws.secret_access_key}")
	private String awsKey;

	/**
	 * Creates Basic AWS Credentials object using awsId and awsKey 
	 * @return BasicAWSCredentials object
	 */
	@Bean
	public BasicAWSCredentials basicAWSCredentials() {
		return new BasicAWSCredentials(awsId, awsKey);
	}

}
