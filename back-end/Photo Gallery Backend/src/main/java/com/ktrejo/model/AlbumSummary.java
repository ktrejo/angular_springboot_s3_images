package com.ktrejo.model;

import java.time.Instant;
import java.util.UUID;

import org.springframework.data.annotation.Id;

public class AlbumSummary {
	
	@Id
	private UUID id;
	
	/**
	 * Name of Album
	 */
	private String albumName;
	
	/**
	 * Timestamp album was created.
	 */
	private Instant timestamp;
	
	/**
	 * Default Constructor
	 */
	public AlbumSummary() {
		super();
	}
	
	/**
	 * Constructor method requiring id, albumName, and timestamp
	 * @param id - UUID generated when the album was created
	 * @param albumName - The name of the album
	 * @param timestamp - The Instant the album was created
	 */
	public AlbumSummary(UUID id, String albumName, Instant timestamp) {
		this.id = id;
		this.albumName = albumName;
		this.timestamp = timestamp;
	}
	
	/**
	 * Constructor method requiring only the album
	 * @param album - Album object
	 */
	public AlbumSummary(Album album) {
		this.id = album.getId();
		this.albumName = album.getAlbumName();
		this.timestamp = album.getTimestamp();
	}

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * @return the albumName
	 */
	public String getAlbumName() {
		return albumName;
	}

	/**
	 * @return the timestamp
	 */
	public Instant getTimestamp() {
		return timestamp;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * @param albumName the albumName to set
	 */
	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Instant timestamp) {
		this.timestamp = timestamp;
	}
	
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		
		if (other instanceof AlbumSummary) {
			AlbumSummary otherPhoto = (AlbumSummary) other;
			
			return this.id.equals(otherPhoto.id);
			
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return this.id.hashCode();
	}

}
