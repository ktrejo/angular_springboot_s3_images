package com.ktrejo.model;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.annotation.Id;

public class UserPhoto {
	
	/**
	 * Id
	 */
	@Id
	private UUID id;
	
	/**
	 * Username
	 */
	private String username;
	
	/**
	 * Name of folder Use to reference S3 data storage
	 */
	private String folderName;
	
	/**
	 * Collection of all photos the user owns
	 */
	private Map<UUID, Photo> allPhotos;
	
	/**
	 * Set of all Album Summary
	 */
	private Set<AlbumSummary> albums;
	
	
	public UUID getId() {
		return id;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getFolderName() {
		return folderName;
	}
	
	public Map<UUID, Photo> getAllPhotos() {
		return allPhotos;
	}
	
	public Set<AlbumSummary> getAlbums() {
		return albums;
	}
	
	public void setId(UUID id) {
		this.id = id;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	
	public void setAllPhotos(Map<UUID, Photo> allPhotos) {
		this.allPhotos = allPhotos;
	}
	
	public void setAlbums(Set<AlbumSummary>albums) {
		this.albums = albums;
	}
	
}
