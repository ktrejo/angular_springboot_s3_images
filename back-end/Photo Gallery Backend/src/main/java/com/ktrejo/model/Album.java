package com.ktrejo.model;

import java.time.Instant;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class Album extends AlbumSummary {
	
	/**
	 * Owner's username
	 */
	private String owner;
	
	/**
	 * Folder path name store on S3
	 */
	private String folderName;
	
	/**
	 * Status for sharing this album
	 */
	private boolean sharedTurnOn;
	
	/**
	 * All users(exluding owner) who is subscribe to this album
	 */
	private Set<String> sharedUsers;
	
	/**
	 * All photos for this current album
	 */
	private Map<UUID, Photo> allPhotos;
		
	/**
	 * Default Constructor
	 */
	public Album() {
		super();
	}
	
	public Album(UserPhoto user, String albumName, Set<String> sharedUsers, 
			Map<UUID, Photo> allPhotos) {
		
		super(UUID.randomUUID(), albumName, Instant.now());
		this.owner = user.getUsername();
		this.sharedTurnOn = false;
		this.folderName = user.getFolderName();
		this.sharedUsers = sharedUsers;
		this.allPhotos = allPhotos;
	}

	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * @return the albumName
	 */
	public String getAlbumName() {
		return super.getAlbumName();
	}

	/**
	 * @return the folderName
	 */
	public String getFolderName() {
		return folderName;
	}

	/**
	 * @return the sharedTurnOn
	 */
	public boolean isSharedTurnOn() {
		return sharedTurnOn;
	}

	/**
	 * @return the sharedUsers
	 */
	public Set<String> getSharedUsers() {
		return sharedUsers;
	}

	/**
	 * @return the allPhotos
	 */
	public Map<UUID, Photo> getAllPhotos() {
		return allPhotos;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * @param folderName the folderName to set
	 */
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	/**
	 * @param sharedTurnOn the sharedTurnOn to set
	 */
	public void setSharedTurnOn(boolean sharedTurnOn) {
		this.sharedTurnOn = sharedTurnOn;
	}

	/**
	 * @param sharedUsers the sharedUsers to set
	 */
	public void setSharedUsers(Set<String> sharedUsers) {
		this.sharedUsers = sharedUsers;
	}

	/**
	 * @param allPhotos the allPhotos to set
	 */
	public void setAllPhotos(Map<UUID, Photo> allPhotos) {
		this.allPhotos = allPhotos;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		
		if (other instanceof Photo) {
			Album otherPhoto = (Album) other;
			
			return (this.getId().equals(otherPhoto.getId()) && 
					this.getAlbumName().equals(otherPhoto.getAlbumName()) &&
					this.owner.equals(otherPhoto.owner) && 
					this.getTimestamp().equals(otherPhoto.getTimestamp()));
			
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return this.getId().hashCode();
	}
}
