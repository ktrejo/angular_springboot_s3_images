package com.ktrejo.model;

import java.time.Instant;
import java.util.UUID;

import org.springframework.data.annotation.Id;

public class Photo {
	
	@Id
	private UUID id;
	
	/**
	 * url Id
	 */
	private String urlId;
	
	/**
	 * Photo Name
	 */
	private String name;
	
	/**
	 * Name of owner
	 */
	private String owner;
	
	/**
	 * Date photo was created
	 */
	private Instant timestamp;
	
	/**
	 * Default Constructor
	 */
	public Photo() {
		super();
	}
	
	public Photo(String photoName, String fileType, String username) {
		this.id = UUID.randomUUID();
		this.urlId = this.id.toString() + fileType;
		this.name = photoName;
		this.owner = username;
		this.timestamp = Instant.now();
	}

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * 
	 * @return urlID
	 */
	public String getUrlId() {
		return urlId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * @return the timestamp
	 */
	public Instant getTimestamp() {
		return timestamp;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(UUID id) {
		this.id = id;
	}
	
	/**
	 * 
	 * @param urlID
	 */
	public void setUrlId(String urlId) {
		this.urlId = urlId;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Instant timestamp) {
		this.timestamp = timestamp;
	}
	
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		
		if (other instanceof Photo) {
			Photo otherPhoto = (Photo) other;
			
			return this.id.equals(otherPhoto.id);
			
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return this.id.hashCode();
	}
}
