package com.ktrejo.dto;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

public class PhotoDTO implements Serializable {

	private static final long serialVersionUID = -4410530719694393374L;
	
	private String keyName;
	private MultipartFile uploadFile;
	
	public PhotoDTO() {
	}

	public String getKeyName() {
		return keyName;
	}

	public MultipartFile getUploadFile() {
		return uploadFile;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public void setUploadFile(MultipartFile uploadFile) {
		this.uploadFile = uploadFile;
	}
	
	

}
