package com.ktrejo.dto;

import java.io.Serializable;
import java.util.List;


public class PhotoListDTO implements Serializable {

	private static final long serialVersionUID = -4536470139942029551L;
	
	private List<String> listOfIds;
	
	public PhotoListDTO() {
	}

	public List<String> getListOfIds() {
		return listOfIds;
	}

	public void setListOfIds(List<String> listOfIds) {
		this.listOfIds = listOfIds;
	}


}
