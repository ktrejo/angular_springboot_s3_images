package com.ktrejo.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ktrejo.converter.dto.UserDTOConverter;
import com.ktrejo.converter.factory.ConverterFactory;
import com.ktrejo.dto.UserDTO;
import com.ktrejo.model.User;


@Component
public class ConverterFacade {

    @Autowired
    private ConverterFactory converterFactory;

    /**
     * Create a new User object using the information from dto
     * @param dto - UserDTO object
     * @return A new User Object.
     */
    public User convert(UserDTO dto) {
    	UserDTOConverter user = (UserDTOConverter) converterFactory
    											.getConverter(dto.getClass());
    	
    	return user.convert(dto);
    }
}
