package com.ktrejo.converter.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.ktrejo.converter.dto.UserDTOConverter;
import com.ktrejo.dto.UserDTO;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;


@Component
public class ConverterFactory {

    private Map<Object, Converter<?, ?>> converters;
    
    @Autowired
    private PasswordEncoder passwordEncoder;

    public ConverterFactory() {

    }

    @PostConstruct
    public void init() {
        converters = new HashMap<>();
        converters.put(UserDTO.class, new UserDTOConverter(passwordEncoder));
    }

    /**
     * 
     * @param type
     * @return Converter for the given type if it exist in map. Otherwise null.
     */
    public Converter<?, ?> getConverter(Object type) {
        return converters.get(type);
    }
}
