package com.ktrejo.converter.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.ktrejo.dto.UserDTO;
import com.ktrejo.model.Authority;
import com.ktrejo.model.User;

import java.util.ArrayList;
import java.util.List;


public class UserDTOConverter implements Converter<UserDTO, User> {
	

	private PasswordEncoder passwordEncoder;
	
	public UserDTOConverter() {
		
	}
	
	@Autowired
	public UserDTOConverter(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

    /**
     * Converts UserDTO object to User object
     * @param dto - UserDTO object containing username and password
     * @return A new User object
     */
	@Override
    public User convert(UserDTO dto) {
        User user = new User();

        user.setUsername(dto.getUsername().toLowerCase());
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setAccountNonExpired(false);
        user.setCredentialsNonExpired(false);
        user.setEnabled(true);

        List<Authority> authorities = new ArrayList<>();
        authorities.add(Authority.ROLE_USER);
        user.setAuthorities(authorities);
        return user;
    }
}
