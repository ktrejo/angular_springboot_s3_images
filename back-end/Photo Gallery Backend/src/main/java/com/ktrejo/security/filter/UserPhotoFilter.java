package com.ktrejo.security.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.web.filter.GenericFilterBean;
import org.springframework.security.core.context.SecurityContextHolder;

import com.ktrejo.model.User;
import com.ktrejo.model.UserPhoto;
import com.ktrejo.repository.UserPhotoRepository;

public class UserPhotoFilter extends GenericFilterBean {
	
	private UserPhotoRepository repository;
	
	public UserPhotoFilter(UserPhotoRepository repository) {
		this.repository = repository;
	}

	/*
	 * Appends user photo object to request if request is authenticate from 
	 * AuthenticationTokenFilter and if user is present in the database
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, 
			FilterChain filterChain)
			throws IOException, ServletException {
		
		if (SecurityContextHolder.getContext().getAuthentication() != null && 
			request.getAttribute("User") != null) {
						
			User user = (User) request.getAttribute("User");
			UserPhoto userPhoto = repository.findById(user.getUserPhotoId()).get();
			
			request.setAttribute("UserPhoto", userPhoto);
			
		}
		
		filterChain.doFilter(request, response);
		
	}

}
