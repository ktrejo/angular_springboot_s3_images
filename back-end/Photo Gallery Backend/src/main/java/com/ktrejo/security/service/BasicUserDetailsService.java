package com.ktrejo.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ktrejo.model.User;
import com.ktrejo.service.UserService;


@Service
public class BasicUserDetailsService implements UserDetailsService {

    private UserService userService;

    @Autowired
    public BasicUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    /*
     * Returns user if username is present in the database. Otherwise, throws 
     * UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findByUsername(username);
        if (user != null) {
            return user;
        } else {
            throw new UsernameNotFoundException("User with username:" + username + " not found");
        }
    }
}
