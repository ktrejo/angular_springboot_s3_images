package com.ktrejo.security.service;


public interface TokenService {

	/*
     * Generate token if username and password is valid. Otherwise throw
     * Service exception
     */
	String getToken(String username, String password);
}
