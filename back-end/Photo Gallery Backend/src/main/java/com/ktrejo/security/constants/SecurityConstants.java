package com.ktrejo.security.constants;


public class SecurityConstants {

	public static final String AUTH_HEADER_NAME = "Authorization";
	public static final String TOKEN_PREFIX = "Bearer ";

    private SecurityConstants() {

    }
}
