package com.ktrejo.security.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ktrejo.model.User;
import com.ktrejo.model.UserAuthentication;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import static com.ktrejo.security.constants.SecurityConstants.AUTH_HEADER_NAME;
import static com.ktrejo.security.constants.SecurityConstants.TOKEN_PREFIX;


@Service
public class JsonWebTokenAuthenticationService implements TokenAuthenticationService {

    @Value("${security.token.secret.key}")
    private String secretKey;

    @Autowired
    private UserDetailsService userDetailsService;

    /*
     * Authenticates token if present in request. Retrieve and return user as 
     * UserAuthentication object if user is present in the database.
     * Returns null otherwise
     */
    @Override
    public Authentication authenticate(HttpServletRequest request) {
        String token = request.getHeader(AUTH_HEADER_NAME);
        
        if (token == null || !token.startsWith(TOKEN_PREFIX)) {
        	return null;
        }
        
        Jws<Claims> tokenData = parseToken(token);
        Calendar calendar = Calendar.getInstance();
        
        if (tokenData != null) {
        	Date tokenExpDate = new Date((long) tokenData.getBody().get("exp"));
            User user = getUserFromToken(tokenData);

            if (calendar.getTime().compareTo(tokenExpDate) <= 0 &&
            		user != null) {
                return new UserAuthentication(user);
            }
        }
        return null;
    }

    private Jws<Claims> parseToken(String token) {
        if (token != null) {
            try {
            	token = token.replace(TOKEN_PREFIX, "");
                return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException
                    | SignatureException | IllegalArgumentException e) {
                return null;
            }
        }
        
        return null;
    }

    private User getUserFromToken(Jws<Claims> tokenData) {
        try {
            return (User) userDetailsService
                    .loadUserByUsername(tokenData.getBody().get("username").toString());
        } catch (UsernameNotFoundException e) {
        	return null;
        }
    }
}
