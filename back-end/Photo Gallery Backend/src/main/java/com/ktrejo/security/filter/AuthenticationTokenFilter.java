package com.ktrejo.security.filter;


import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import com.ktrejo.security.service.TokenAuthenticationService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


public class AuthenticationTokenFilter extends GenericFilterBean {

    private TokenAuthenticationService authenticationService;
    
    public AuthenticationTokenFilter(TokenAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    /*
     * Authenticate user if token is in request. If token is present and 
     * authenticate then filter will append user object in request
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain filterChain)
            throws IOException, ServletException {
    	
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        Authentication authentication = authenticationService.authenticate(httpRequest);

        if (authentication != null) {
        	SecurityContextHolder.getContext().setAuthentication(authentication);
            request.setAttribute("User", authentication.getDetails());
        }
        
        filterChain.doFilter(request, response);
        //SecurityContextHolder.getContext().setAuthentication(null);
        
    }
    
}
