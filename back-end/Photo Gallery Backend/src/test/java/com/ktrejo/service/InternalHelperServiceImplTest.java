package com.ktrejo.service;

import static org.junit.jupiter.api.Assertions.*;

import java.util.UUID;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class InternalHelperServiceImplTest {
	
	private InternalHelperService service = new InternalHelperServiceImpl();

	@DisplayName("Test String to UUID conversion")
	@Test
	void testStringToUUID() {
		String validUUID = "5c5e227a-811d-11ea-bc55-0242ac130003";
		String invalidUUID = "invalid";
		UUID uuid = UUID.fromString(validUUID);
		
		assertEquals(uuid, service.StringToUUID(validUUID));
		assertEquals(null, service.StringToUUID(invalidUUID));
		assertEquals(null, service.StringToUUID(null));
	}

}
