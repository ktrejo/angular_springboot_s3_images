package com.ktrejo.service;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.ArgumentMatchers;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;

@SpringBootTest
class S3ServiceImplTest {
	
	@Mock
	AmazonS3 s3client;
	
	@Mock
	MultipartFile file = new MockMultipartFile("file", new byte[10]);
	
	@InjectMocks
	S3Service service = new S3ServiceImpl();
	
	@BeforeEach
	void setMockOutput() {

	}

	@DisplayName("Test UploadImageFile method")
	@Test
	void testUploadImageFile() {
		
		String key = "key";
		String[][] tags = new String[0][0];
		
		when(s3client.putObject(
				ArgumentMatchers.anyString(),
				ArgumentMatchers.anyString(),
				ArgumentMatchers.any(InputStream.class),
				ArgumentMatchers.any(ObjectMetadata.class))
			).thenReturn(null);
		
		when(file.getContentType()).thenReturn("img/jpg");
		
		assertEquals(true, service.uploadImageFile(key, tags, file));
		assertEquals(false, service.uploadImageFile(null, tags, file));
		assertEquals(false, service.uploadImageFile(key, null, file));
		assertEquals(false, service.uploadImageFile(key, tags, null));
		
	}

	@DisplayName("Test copying and moving an image file")
	@Test
	void testCopyImageFile() {
		String src = "src file";
		String dest = "dest location";
		when(s3client.copyObject(
				ArgumentMatchers.any(CopyObjectRequest.class))
			).thenReturn(null);
		
		assertEquals(true, service.copyImageFile(src, dest));
	}

	@DisplayName("Test deleting an image")
	@Test
	void testDeleteImageFile() {
		String key = "key";
		doNothing()
			.when(s3client)
			.deleteObject(ArgumentMatchers.any(DeleteObjectRequest.class));
		
		assertEquals(true, service.deleteImageFile(key));
		assertEquals(false, service.deleteImageFile(null));
	}

	@DisplayName("Test deleting multiple images")
	@Test
	void testDeleteMultiImage() {
		List<String> photoToDelete = new ArrayList<String>();
		when(s3client.deleteObjects(
				ArgumentMatchers.any(DeleteObjectsRequest.class))
			).thenReturn(null);
		
		assertEquals(true, service.deleteMultiImage(photoToDelete));
		assertEquals(false, service.deleteMultiImage(null));
	}

}
