package com.ktrejo.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.ArgumentMatchers;
import org.slf4j.Logger;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest
class ResponseGeneratorServiceJSONTest {
	
	@Mock
	private Logger log;
	
	@InjectMocks
	ResponseGeneratorService service = new ResponseGeneratorServiceJSON();

	@DisplayName("Test generating a response with valid parameters")
	@Test
	void testGenerateResponseValid() {
		HttpStatus status = HttpStatus.OK;
		Object[][] body = new Object[][] {{"KEY", "VALUE"}};
		Map<String, String> responseBody = new HashMap<String, String>();
		responseBody.put("KEY", "VALUE");
		ResponseEntity<?> response = new ResponseEntity<>(responseBody, status);
			
		assertEquals(response, service.generateResponse(status, body));
	}
	
	@DisplayName("Test generating a response with incorrect parameters")
	@Test
	void testGenerateResponseWrong() {
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		Object[][] body = new Object[][] {{"KEY"}};
		ResponseEntity<?> response = new ResponseEntity<>(status);
	
		doNothing().when(log).error(ArgumentMatchers.anyString());
		
		assertEquals(response, service.generateResponse(status, body));
	}

}
