package com.ktrejo.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.ktrejo.model.Album;
import com.ktrejo.model.AlbumSummary;
import com.ktrejo.model.Photo;
import com.ktrejo.model.UserPhoto;
import com.ktrejo.repository.AlbumRepository;
import com.ktrejo.repository.UserPhotoRepository;

@SpringBootTest
class AlbumServiceImplTest {
	
	@Mock
	private S3Service s3Service;
	
	@Mock
	private AlbumRepository albumRepository;
	
	@Mock
	private UserPhotoRepository userPhotoRepository;
	
	@InjectMocks
	private AlbumService service = new AlbumServiceImpl();
	
	private void init() {
		when(albumRepository.save(ArgumentMatchers.any())).thenReturn(null);
	}

	@DisplayName("Test creating an album")
	@Test
	void testCreateAlbum() {
		init();
		UserPhoto userPhoto = userPhotoSample1();
		String albumName = "testAlbum";
		Album album = service.createAlbum(userPhoto, albumName);
		
		assertEquals(1, userPhoto.getAlbums().size());
		assertEquals(albumName, album.getAlbumName());
	}

	@DisplayName("Test deleting an album")
	@Test
	void testDeleteAlbum() {
		init();
		UserPhoto userPhoto = userPhotoSample1();
		String albumName = "testAlbum";
		Album album = service.createAlbum(userPhoto, albumName);
		
		assertEquals(1, userPhoto.getAlbums().size());
		service.deleteAlbum(userPhoto, album);
		assertEquals(0, userPhoto.getAlbums().size());
	}

	@DisplayName("Test getting an album's info")
	@Test
	void testGetAlbum() {
		init();
		UserPhoto userPhoto = userPhotoSample1();
		String albumName = "testAlbum";
		Album album = service.createAlbum(userPhoto,  albumName);
		Optional<Album> returnValue = Optional.of(album);
		UUID id = UUID.randomUUID();
		
		while (album.getId().equals(id)) {
			id = UUID.randomUUID();
		}
		
		when(albumRepository.existsById(ArgumentMatchers.eq(id))).thenReturn(false);
		when(albumRepository.existsById(ArgumentMatchers.eq(album.getId())))
			.thenReturn(true);
		
		when(albumRepository.findById(ArgumentMatchers.any())).thenReturn(returnValue);
		
		assertEquals(null, service.getAlbum(null));
		assertEquals(null, service.getAlbum(id));
		assertEquals(album, service.getAlbum(album.getId()));
		
	}

	@DisplayName("Test enabling sharing mode on album")
	@Test
	void testToggleShare() {
		init();
		UserPhoto userPhoto = userPhotoSample1();
		String albumName = "testAlbum";
		Album album = service.createAlbum(userPhoto, albumName);
		
		assertEquals(false, album.isSharedTurnOn());
		service.toggleShare(userPhoto, album);
		assertEquals(true, album.isSharedTurnOn());
	}

	@DisplayName("Test adding photo(s) to an album")
	@Test
	void testAddPhotos() {
		init();
		UserPhoto userPhoto = userPhotoSample2();
		Album album = createAlbum(userPhoto, "testAblum");
		List<UUID> ids = new ArrayList<UUID>(userPhoto.getAllPhotos().keySet());
		
		when(s3Service.copyImageFile(ArgumentMatchers.any(), ArgumentMatchers.any()))
			.thenReturn(true);
		
		assertEquals(0, album.getAllPhotos().size());
		service.addPhotos(userPhoto, ids, album);
		assertEquals(ids.size(), album.getAllPhotos().size());
		
	}

	@DisplayName("Test removing photo(s) from an album")
	@Test
	void testRemovePhotos() {
		init();
		UserPhoto userPhoto = userPhotoSample2();
		Album album = createAlbum(userPhoto, "testAblum");
		List<UUID> ids = new ArrayList<UUID>(userPhoto.getAllPhotos().keySet());
		
		when(s3Service.copyImageFile(ArgumentMatchers.any(), ArgumentMatchers.any()))
			.thenReturn(true);
		
		service.addPhotos(userPhoto, ids, album);
		assertEquals(ids.size(), album.getAllPhotos().size());
		service.removePhotos(userPhoto, ids, album);
		assertEquals(0, album.getAllPhotos().size());
	}

	@DisplayName("Test adding user(s) to an album")
	@Test
	void testAddUserToSharedAlbum() {
		init();
		UserPhoto user = createUserPhoto("user");
		UserPhoto otherUser = createUserPhoto("otherUser");
		String notInDatabase = "unkownUser";
		Album sharedAlbum = createAlbum(user, "sharedAlbum");
		sharedAlbum.setSharedTurnOn(true);
		
		when(userPhotoRepository.findByUsername("otherUser")).thenReturn(otherUser);
		when(userPhotoRepository.findByUsername(notInDatabase)).thenReturn(null);
		when(userPhotoRepository.save(ArgumentMatchers.any())).thenReturn(otherUser);
		
		service.addUserToSharedAlbum(user, otherUser.getUsername(), sharedAlbum);
		assertTrue(sharedAlbum.getSharedUsers().contains(otherUser.getUsername()));
		
		service.addUserToSharedAlbum(user, notInDatabase, sharedAlbum);
		assertFalse(sharedAlbum.getSharedUsers().contains(notInDatabase));
	}

	@DisplayName("Test removing user(s) from an album")
	@Test
	void testRemoveUserFromSharedAlbum() {
		init();
		UserPhoto user = createUserPhoto("user");
		UserPhoto otherUser = createUserPhoto("otherUser");
		String notInDatabase = "unkownUser";
		Album sharedAlbum = createAlbum(user, "sharedAlbum");
		sharedAlbum.setSharedTurnOn(true);
		
		when(userPhotoRepository.findByUsername("otherUser")).thenReturn(otherUser);
		when(userPhotoRepository.findByUsername(notInDatabase)).thenReturn(null);
		when(userPhotoRepository.save(ArgumentMatchers.any())).thenReturn(otherUser);
		
		service.addUserToSharedAlbum(user, otherUser.getUsername(), sharedAlbum);
		assertTrue(sharedAlbum.getSharedUsers().contains(otherUser.getUsername()));
		
		service.removeUserFromSharedAlbum(user, otherUser.getUsername(), sharedAlbum);
		assertFalse(sharedAlbum.getSharedUsers().contains(otherUser.getUsername()));
	}
		
	private UserPhoto createUserPhoto(String username) {
		if (username == null) {
			return null;
		}
		
		UserPhoto userPhoto = new UserPhoto();
		userPhoto.setId(UUID.randomUUID());
		userPhoto.setUsername(username);
		userPhoto.setFolderName(userPhoto.getId().toString());
		userPhoto.setAllPhotos(new HashMap<UUID, Photo>());
		userPhoto.setAlbums(new HashSet<AlbumSummary>());
		
		return userPhoto;
	}
	
	private Album createAlbum(UserPhoto user, String nameOfAlbum) {
		HashSet<String> set = new HashSet<String>();
		HashMap<UUID, Photo> map = new HashMap<UUID, Photo>();
		Album album = new Album(user, nameOfAlbum, set, map);
		user.getAlbums().add(new AlbumSummary(album));
		
		return album;
	}
	
	private UserPhoto userPhotoSample1() {
		return createUserPhoto("test1");
	}
	
	private UserPhoto userPhotoSample2() {
		UserPhoto user = createUserPhoto("test3");
		Photo photo1 = new Photo("Photo1", "jpeg", user.getUsername());
		Photo photo2 = new Photo("Photo2", "jpg", user.getUsername());
		Photo photo3 = new Photo("Photo3", "png", user.getUsername());
		
		user.getAllPhotos().put(photo1.getId(), photo1);
		user.getAllPhotos().put(photo2.getId(), photo2);
		user.getAllPhotos().put(photo3.getId(), photo3);
		
		return user;
	}

}
