package com.ktrejo.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.ArgumentMatchers;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.ktrejo.model.AlbumSummary;
import com.ktrejo.model.Photo;
import com.ktrejo.model.User;
import com.ktrejo.model.UserPhoto;
import com.ktrejo.repository.UserPhotoRepository;

@SpringBootTest
class PhotoServiceImplTest {
	
	@Mock
	private S3Service s3Service;
	
	@Mock
	private UserPhotoRepository photoRepo;
	
	@Mock
	private AlbumService albumService;
	
	@InjectMocks
	PhotoService service = new PhotoServiceImpl();
	
	void init() {
		when(s3Service.uploadImageFile(
				ArgumentMatchers.anyString(), 
				ArgumentMatchers.any(), 
				ArgumentMatchers.any())
			).thenReturn(true);
		
		when(s3Service.deleteImageFile(ArgumentMatchers.anyString())).thenReturn(true);
		when(s3Service.deleteMultiImage(ArgumentMatchers.anyList())).thenReturn(true);
		when(photoRepo.save(ArgumentMatchers.any())).thenReturn(null);
		doNothing().when(photoRepo).insertPhoto(
				ArgumentMatchers.any(),
				ArgumentMatchers.any()
		);
	}
	

	@DisplayName("Test creating a UserPhoto from a given user")
	@Test
	void testCreateUserPhoto() {
		init();
		
		User user = createUser("testUser");
		UserPhoto userPhoto = createUserPhoto(user);
		UserPhoto testUserPhoto = service.createUserPhoto(user);
		UUID testID = testUserPhoto.getId();
		
		assertEquals(null, service.createUserPhoto(null));
		assertEquals(userPhoto.getUsername(), testUserPhoto.getUsername());
		assertEquals(testID.toString(), testUserPhoto.getFolderName());
		assertEquals(0, testUserPhoto.getAllPhotos().size());
		assertEquals(0, testUserPhoto.getAlbums().size());
	}

	@DisplayName("Test adding a photo")
	@Test
	void testAddPhoto() {
		init();
		
		UserPhoto user = createUserPhoto("test");
		String photoName = "picture.png";
		MultipartFile file = new MockMultipartFile("image", new byte[10]);
		
		assertEquals(0, user.getAllPhotos().size());
		assertEquals(1, service.addPhoto(user, photoName, file).getAllPhotos().size());
		assertEquals(1, service.addPhoto(user, photoName, null).getAllPhotos().size());
		assertEquals(1, service.addPhoto(user, "Not valid", file).getAllPhotos().size());
	}

	@DisplayName("Test deleting a photo")
	@Test
	void testDeletePhoto() {
		init();
		
		UserPhoto userPhoto = userPhotoSample();
		ArrayList<UUID> keys = new ArrayList<UUID>(userPhoto.getAllPhotos().keySet());
		UUID firstKey = keys.get(0);
		
		// Remove photo
		service.deletePhoto(userPhoto, firstKey);
		assertEquals(keys.size() - 1, userPhoto.getAllPhotos().size());
		
		// Photo already removed. Photo id does not exist in userPhoto.
		service.deletePhoto(userPhoto, firstKey);
		assertEquals(keys.size() - 1, userPhoto.getAllPhotos().size());
		
	}

	@DisplayName("Test multiple a photos at once")
	@Test
	void testDeleteMultiPhoto() {
		init();
		
		UserPhoto userPhoto = userPhotoSample();
		ArrayList<UUID> keys = new ArrayList<UUID>(userPhoto.getAllPhotos().keySet());
		
		service.deleteMultiPhoto(userPhoto, keys);
		assertEquals(0, userPhoto.getAllPhotos().size());
	}
	
	private User createUser(String username) {
		if (username == null) {
			return null;
		}
			User user = new User();
			user.setUsername(username);
			
			return user;
	}
	
	private UserPhoto createUserPhoto(User user) {
		return createUserPhoto(user.getUsername());
	}
	
	private UserPhoto createUserPhoto(String username) {
		if (username == null) {
			return null;
		}
		
		UserPhoto userPhoto = new UserPhoto();
		userPhoto.setId(UUID.randomUUID());
		userPhoto.setUsername(username);
		userPhoto.setFolderName(userPhoto.getId().toString());
		userPhoto.setAllPhotos(new HashMap<UUID, Photo>());
		userPhoto.setAlbums(new HashSet<AlbumSummary>());
		
		return userPhoto;
	}
	
	private UserPhoto userPhotoSample() {
		UserPhoto user = createUserPhoto("test1");
		MultipartFile file = new MockMultipartFile("image", new byte[10]);
		service.addPhoto(user, "picture1.png", file);
		service.addPhoto(user, "picture2.png", file);
		service.addPhoto(user, "picture3.png", file);
		service.addPhoto(user, "image1.png", file);
		service.addPhoto(user, "image2.jpg", file);
		service.addPhoto(user, "image3.jpeg", file);
		
		return user;
	}

}
